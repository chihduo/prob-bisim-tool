name := "probsim"

version := "0.1"

scalaVersion := "2.12.7"

mainClass in (Compile, run) := Some("probsim.Main")

compileOrder := CompileOrder.Mixed

// resolvers += "uuverifiers" at "http://logicrunch.it.uu.se:4096/~wv/maven/"

libraryDependencies += "org.apache.logging.log4j" % "log4j-api" % "2.11.1"
libraryDependencies += "org.apache.logging.log4j" % "log4j-core" % "2.11.1"

libraryDependencies += "org.sat4j" % "org.sat4j.core" % "2.3.1"
libraryDependencies += "org.slf4j" % "slf4j-api" % "1.7.25"


// libraryDependencies += "uuverifiers" %% "princess" % "2017-07-17"
// libraryDependencies += "uuverifiers" %% "princess" % "nightly-SNAPSHOT"


