package common.finiteautomata;

import common.bellmanford.DirectedEdge;
import common.bellmanford.DirectedEdgeWithInputOutput;
import common.bellmanford.EdgeWeightedDigraph;

import java.io.*;
import java.util.*;

public class MonaUtility {

  static final String MONA_PATH = "mona";

  /**
   * Note that this method will remove a dummy don't-care state (state 0)
   * and make state 1 the true initial state of the produced DFA.
   * This adjustment is made so that the load/save methods
   * (`loadMonaDFA` and `writeMonaDFA`) are consistent.
   */
  public static Automata loadMonaDFA(String fileName, boolean isRelation)
          throws IOException {
    File file = new File(fileName);
    Scanner input = new Scanner(file);
    State[] states = null;
    int initStateId = 0, numLabels = -1;
    String[] finalStates = null;
    while (input.hasNextLine()) {
      String line = input.nextLine();
      if (line.length() == 0) continue;
      String[] tokens = line.split(" ");
      switch (tokens[0]) {
        case "Initial": /* Initial state: 0 */
          initStateId = Integer.parseInt(tokens[tokens.length - 1]);
          assert (initStateId == 0);
          break;
        case "Accepting": /* Eg. Accepting states: 0 1 2 ... */
          finalStates = Arrays.copyOfRange(tokens, 2, tokens.length);
          break;
        case "Rejecting": /* Eg. Rejecting states: 0 1 2 ... */
          /* non-accepting states are automatically rejecting */
          break;
        case "Automaton": /* Eg. Automaton has 10 states */
          /* note that state 0 is removed */
          int numStates = Integer.parseInt(tokens[2]) - 1;
          states = new State[numStates];
          for (int i = 0; i < states.length; i++)
            states[i] = new State(i);
          break;
        case "Transitions:":
          assert (states != null);
          while (input.hasNextLine()) {
            // Eg. State 2: 1X0 -> state 3
            String ln = input.nextLine();
            String[] tk = ln.split(" ");
            int sid = Integer.parseInt(tk[1].substring(0, tk[1].length() - 1)) - 1;
            int to = Integer.parseInt(tk[tk.length - 1]) - 1;
            if (sid == -1) {
              assert (to == 0);
              continue;
            }
            assert (to >= 0);
            State from = states[sid];
            String labels = tk[2];
            if (numLabels < 0) {
              assert (labels.length() < Math.log(Integer.MAX_VALUE) / Math.log(2));
              int res = 1;
              for (int i = 1, exp = labels.length(); i <= exp; i++) {
                res *= 2;
              }
              numLabels = res;
            }
            addTrans(from, to, 0, labels.toCharArray(), isRelation);
          }
          break;
        default:
          //System.out.println("Skip line: " + line);
      }
    }
    assert (states != null && finalStates != null);
    Automata aut = new Automata(initStateId, states, numLabels);
    List<Integer> finalStatesIds = new ArrayList<>(finalStates.length);
    for (String id : finalStates) {
      int sid = Integer.parseInt(id) - 1;
      assert (sid >= 0);
      finalStatesIds.add(sid);
    }
    aut.setAcceptingStates(finalStatesIds);
    return AutomataConverter.minimise(aut);
    //return aut;
  }

  private static void addTrans(State from, int sid, int pos, char[] labels, boolean isRelation) {
    if (pos >= labels.length) {
      from.addTrans(binToInt(labels, isRelation), sid);
      return;
    }
    if (labels[pos] == 'X') {
      labels[pos] = '0';
      addTrans(from, sid, pos + 1, labels, isRelation);
      labels[pos] = '1';
      addTrans(from, sid, pos + 1, labels, isRelation);
      labels[pos] = 'X';
    } else {
      addTrans(from, sid, pos + 1, labels, isRelation);
    }
  }

  /**
   * Note that the bit-vector is in reverse order (LSB first) of
   * the binary encoding of the integer label (MSB first).
   * The characters of the vector can be {0, 1, X} (X is treated as 0).
   * When the automaton encodes a relation:
   * -  The bit-vector is (LSB) w1 :: w2 (MSB)
   * -  The binary encoding is (MSB) w1 :: w2 (LSB)
   * When the automaton encodes a language:
   * -  The bit-vector is (LSB) w1 :: 0 (MSB)
   * -  The binary encoding is (MSB) 0 :: w1 (LSB)
   */
  private static int binToInt(char[] bin, boolean isRelation) {
    int ret = 0, mid = bin.length;
    if (isRelation) {
      assert (bin.length % 2 == 0);
      mid = bin.length / 2;
    }
    /* encode w1 */
    for (int i = mid - 1; i >= 0; i--) {
      ret = (ret << 1) + (bin[i] == '1' ? 1 : 0);
    }
    /* encode w2 */
    for (int i = bin.length - 1; i >= mid; i--) {
      ret = (ret << 1) + (bin[i] == '1' ? 1 : 0);
    }
    return ret;
  }

  /**
   * Note that the bit-vector is in reverse order (LSB first) of
   * the binary encoding of the integer label (MSB first).
   * When the automaton encodes a relation:
   * -  The bit-vector is (LSB) w1 :: w2 (MSB)
   * -  The binary encoding is (MSB) w1 :: w2 (LSB)
   * When the automaton encodes a language:
   * -  The bit-vector is (LSB) w1 :: 0 (MSB)
   * -  The binary encoding is (MSB) 0 :: w1 (LSB)
   **/
  private static String labelToBin(int label, int bitWidth, int alphabetSize, boolean isRelation) {
    if (isRelation) bitWidth = bitWidth * 2;
    int w1 = isRelation ? (label / alphabetSize) : label;
    int w2 = isRelation ? (label % alphabetSize) : 0;
    char[] bin = new char[bitWidth];
    /* encode w1 */
    for (int i = 0; i < bitWidth; i++) {
      bin[i] = w1 % 2 == 0 ? '0' : '1';
      w1 >>>= 1;
    }
    /* encode w2 if automaton encodes a relation */
    if (isRelation) {
      for (int i = bitWidth / 2; i < bitWidth; i++) {
        bin[i] = w2 % 2 == 0 ? '0' : '1';
        w2 >>>= 1;
      }
    }
    return new String(bin);
  }

  /**
   * Note that this method will append a dummy don't-care state after
   * the initial state to make the produced DFA compatible with
   * the format of Mona DFA (cf. the manual)
   */
  public static void writeMonaDFA(String filename, Automata aut, int bitWidth, boolean isRelation)
          throws IOException {
    writeMonaDFA(filename, aut, bitWidth, isRelation, false);
  }

  public static void writeMonaDFA(String filename, EdgeWeightedDigraph aut, int bitWidth, boolean isRelation)
          throws IOException {
    writeMonaDFA(filename, aut, bitWidth, isRelation, false);
  }

  public static void writeMonaDFA(String filename, Automata aut,
                                  int bitWidth, boolean isRelation, boolean debugMode)
          throws IOException {
    BufferedWriter writer;
    if (!debugMode) {
      ProcessBuilder builder = new ProcessBuilder("translator", filename);
      Process process = builder.start();
      OutputStream stdin = process.getOutputStream();
      writer = new BufferedWriter(new OutputStreamWriter(stdin));
    } else {
      writer = new BufferedWriter(new PrintWriter(System.out));
    }
    int initState = aut.getInitState();
    State[] states = aut.getStates();
    Set<Integer> acc = aut.getAcceptingStates();
    //Arrays.sort(states);
    writer.write(states.length + "\n"); /* number of states */
    writer.write((isRelation ? bitWidth * 2 : bitWidth) + "\n");
    for (int i = 0; i < states.length; i++) {
      /* swap state id if initial-state id is nonzero */
      int sid = i;
      if (sid == 0) sid = initState;
      else if (sid == initState) sid = 0;
      State from = states[sid];
      Set<Integer> labels = from.getOutgoingLabels();
      String[] lines = new String[labels.size()];
      int numLines = 0;
      for (Integer label : labels) {
        String bin = labelToBin(label, bitWidth, 1 << bitWidth, isRelation);
        for (Integer to : from.getDest(label)) {
          /* swap state id if initial-state id is nonzero */
          if (to == 0) to = initState;
          else if (to == initState) to = 0;
          lines[numLines] = to + "\n" + bin;
          numLines++;
        }
      }
      if (numLines > 0) {
        writer.write("# state " + from.getId() + " has " + numLines + " outgoing transitions to " + from.getDest() + "\n");
        writer.write(numLines + "\n");
        for (int j = 0; j < numLines; j++) {
          writer.write(lines[j] + "\n");
        }
      } else {
        writer.write("# state " + from.getId() + " is a dead-end\n");
        writer.write("0\n");
      }
    }
    char[] statuses = new char[states.length + 1];
    for (int i = 0; i < states.length; i++) statuses[i] = '-';
    for (Integer j : acc) statuses[j] = '+';
    statuses[states.length] = '\n';
    writer.write(statuses);
    writer.flush();
    writer.close();
  }

  public static void writeMonaDFA(String filename, EdgeWeightedDigraph aut,
                                  int bitWidth, boolean isRelation, boolean debugMode)
          throws IOException {
    BufferedWriter writer;
    if (!debugMode) {
      ProcessBuilder builder = new ProcessBuilder("translator", filename);
      Process process = builder.start();
      OutputStream stdin = process.getOutputStream();
      writer = new BufferedWriter(new OutputStreamWriter(stdin));
    } else {
      writer = new BufferedWriter(new PrintWriter(System.out));
    }
    int numStates = aut.V();
    int initState = aut.getInitState();
    Set<Integer> acc = aut.getAcceptingStates();
    //Arrays.sort(states);
    writer.write(numStates + "\n"); /* number of states */
    writer.write((isRelation ? bitWidth * 2 : bitWidth) + "\n");
    List<String> lines = new ArrayList<>();
    for (int i = 0; i < numStates; i++) {
      /* swap state id if initial-state id is nonzero */
      int sid = i;
      if (sid == 0) sid = initState;
      else if (sid == initState) sid = 0;
      Iterable<DirectedEdge> edges = aut.adj(sid);
      int numLines = 0;
      for (DirectedEdge e : edges) {
        assert (e.from() == sid);
        DirectedEdgeWithInputOutput edge = (DirectedEdgeWithInputOutput) e;
        int left = edge.getInput();
        int right = edge.getOutput();
        int label = (left << bitWidth) + right;
        String bin = labelToBin(label, bitWidth, 1 << bitWidth, isRelation);
        int dest = edge.to();
        /* swap state id if initial-state id is nonzero */
        if (dest == 0) dest = initState;
        else if (dest == initState) dest = 0;
        lines.add(dest + "\n" + bin);
        numLines++;
      }
      if (numLines > 0) {
        writer.write("# state " + sid + " has " + numLines + " outgoing transitions\n");
        writer.write(numLines + "\n");
        for (String line : lines) {
          writer.write(line + "\n");
        }
        lines.clear();
      } else {
        writer.write("# state " + sid + " is a dead-end\n");
        writer.write("0\n");
      }
    }
    char[] statuses = new char[numStates + 1];
    for (int i = 0; i < numStates; i++) statuses[i] = '-';
    for (Integer j : acc) statuses[j] = '+';
    statuses[numStates] = '\n';
    writer.write(statuses);
    writer.flush();
    writer.close();
  }

  public static boolean getCexFromTeacher(String filename, int bitWidth,
                                          List<Integer> cex, boolean isRelation)
          throws IOException {
    if (isRelation) bitWidth = bitWidth * 2;
    ProcessBuilder builder = new ProcessBuilder(MONA_PATH, "-q", filename);
    Process process = builder.start();
    Scanner scanner = new Scanner(process.getInputStream());
    while (scanner.hasNextLine()) {
      String line = scanner.nextLine();
      String[] tokens = line.split(" ");
      if (tokens.length > 2 && tokens[2].equals("unsatisfiable"))
        return true;
      if (tokens.length > 1 && tokens[1].equals("satisfying")) {
        int currentRow = 0;
        char[][] rows = new char[bitWidth][];
        while (scanner.hasNextLine()) {
          line = scanner.nextLine();
          tokens = line.split(" +");
          rows[currentRow] = tokens[2].toCharArray();
          currentRow++;
          if (currentRow >= bitWidth)
            break;
        }
        for (int col = 0, numColumns = rows[0].length; col < numColumns; col++) {
          char[] vector = new char[bitWidth];
          for (int row = 0; row < bitWidth; row++) {
            vector[row] = rows[row][col];
          }
          int pair = binToInt(vector, isRelation);
          cex.add(pair);
        }
      }
    }
    if (cex.size() == 0) {
      /* something went wrong with Mona - dump error message */
      builder = new ProcessBuilder(MONA_PATH, "-q", filename);
      process = builder.start();
      scanner = new Scanner(process.getInputStream());
      while (scanner.hasNextLine()) {
        System.out.println(scanner.nextLine());
      }
      return false;
    }
    return true;
  }

  private static int statePair(int s1, int s2, int alphaSize) {
    return s1 + s2 * alphaSize;
  }

  /**
   * Note that the bit-vector is in reverse order (LSB first) of
   * the binary encoding of the integer label (MSB first).
   * -  The bit-vector is (LSB) w1 :: w2 :: u1 :: u2 (MSB)
   * -  The binary encoding is (MSB) w1 :: w2 :: u1 :: u2 (LSB)
   **/
  public static Automata getFourTupleRelation(Automata aut, int bitWidth) {
    int init = aut.getInitState();
    State[] st = aut.getStates();
    Set<Integer> acc = aut.getAcceptingStates();
    int numLabelsF = aut.getNumLabels() << bitWidth;
    int initStateF = statePair(init, init, st.length);
    HashSet<Integer> accStatesF = new HashSet<>();
    Automata ret = new Automata(initStateF, numLabelsF,
            aut.getNumLabels() * aut.getNumLabels());
    for (State s1 : st) {
      for (State s2 : st) {
        int from = statePair(s1.getId(), s2.getId(), st.length);
        if (acc.contains(s1.getId()) && acc.contains(s2.getId()))
          accStatesF.add(from);
        for (int l1 : s1.getOutgoingLabels()) {
          for (int l2 : s2.getOutgoingLabels()) {
            for (int t1 : s1.getDest(l1)) {
              for (int t2 : s2.getDest(l2)) {
                int to = statePair(t1, t2, st.length);
                int label = (l1 << bitWidth) | l2;
                ret.addTrans(from, label, to);
              }
            }
          }
        }
      }
    }
    ret.setAcceptingStates(accStatesF);
    return ret;
  }
}
