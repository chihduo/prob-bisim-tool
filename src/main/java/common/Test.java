package common;

import common.bellmanford.DirectedEdgeWithInputOutput;
import common.bellmanford.EdgeWeightedDigraph;
import common.finiteautomata.Automata;
import common.finiteautomata.MonaUtility;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class Test {

  public static Automata getAutomaton() throws IOException {

    int bitWidth = 3;
    int initState = 0;
    int numStates = 5;
    int numLabels = 1 << (bitWidth * 2);

    Automata aut = new Automata(initState, numStates, numLabels);

    Set<Integer> acc = new HashSet<>();
    acc.add(0);
    acc.add(1);
    acc.add(2);
    acc.add(4);
    aut.setAcceptingStates(acc);

    aut.addTrans(0, (7<<bitWidth)+7, 0);
    aut.addTrans(0, (5<<bitWidth)+6, 1);
    aut.addTrans(0, (6<<bitWidth)+5, 1);
    aut.addTrans(0, (0<<bitWidth)+0, 2);
    aut.addTrans(0, (1<<bitWidth)+1, 2);
    aut.addTrans(0, (2<<bitWidth)+2, 2);
    aut.addTrans(0, (3<<bitWidth)+3, 2);
    aut.addTrans(0, (4<<bitWidth)+4, 2);
    aut.addTrans(0, (5<<bitWidth)+5, 4);
    aut.addTrans(0, (6<<bitWidth)+6, 4);

    aut.addTrans(1, (5<<bitWidth)+5, 1);
    aut.addTrans(1, (5<<bitWidth)+6, 1);
    aut.addTrans(1, (6<<bitWidth)+5, 1);
    aut.addTrans(1, (6<<bitWidth)+6, 1);

    aut.addTrans(2, (0<<bitWidth)+0, 0);
    aut.addTrans(2, (5<<bitWidth)+5, 0);
    aut.addTrans(2, (6<<bitWidth)+6, 0);
    aut.addTrans(2, (5<<bitWidth)+6, 1);
    aut.addTrans(2, (6<<bitWidth)+5, 1);
    aut.addTrans(2, (1<<bitWidth)+1, 2);
    aut.addTrans(2, (2<<bitWidth)+2, 2);
    aut.addTrans(2, (3<<bitWidth)+3, 2);
    aut.addTrans(2, (4<<bitWidth)+4, 2);
    aut.addTrans(2, (3<<bitWidth)+4, 3);
    aut.addTrans(2, (4<<bitWidth)+3, 3);
    aut.addTrans(2, (7<<bitWidth)+7, 4);

    aut.addTrans(3, (0<<bitWidth)+0, 0);
    aut.addTrans(3, (1<<bitWidth)+7, 1);
    aut.addTrans(3, (3<<bitWidth)+4, 1);
    aut.addTrans(3, (4<<bitWidth)+3, 1);
    aut.addTrans(3, (7<<bitWidth)+1, 1);
    aut.addTrans(3, (1<<bitWidth)+1, 3);

    aut.addTrans(4, (5<<bitWidth)+5, 0);
    aut.addTrans(4, (6<<bitWidth)+6, 0);
    aut.addTrans(4, (5<<bitWidth)+6, 1);
    aut.addTrans(4, (6<<bitWidth)+5, 1);
    aut.addTrans(4, (0<<bitWidth)+0, 4);
    aut.addTrans(4, (1<<bitWidth)+1, 4);
    aut.addTrans(4, (1<<bitWidth)+3, 4);
    aut.addTrans(4, (2<<bitWidth)+2, 4);
    aut.addTrans(4, (3<<bitWidth)+1, 4);
    aut.addTrans(4, (3<<bitWidth)+3, 4);
    aut.addTrans(4, (4<<bitWidth)+4, 4);
    aut.addTrans(4, (7<<bitWidth)+7, 4);

    return aut;
  }

  static DirectedEdgeWithInputOutput edge(int from, int in, int out, int to) {
    return new DirectedEdgeWithInputOutput(from, to, in, out);
  }

  public static EdgeWeightedDigraph getDigraph() throws IOException {

    int initState = 0;
    int numStates = 5;

    Set<Integer> acc = new HashSet<>();
    acc.add(0);
    acc.add(1);
    acc.add(2);
    acc.add(3);

    EdgeWeightedDigraph graph = new EdgeWeightedDigraph(numStates, initState, acc);

    graph.addEdge(edge(0, 5, 5, 0));
    graph.addEdge(edge(0, 7, 7, 0));
    graph.addEdge(edge(0, 0, 0, 3));
    graph.addEdge(edge(0, 1, 1, 3));
    graph.addEdge(edge(0, 2, 2, 3));
    graph.addEdge(edge(0, 3, 3, 3));
    graph.addEdge(edge(0, 4, 4, 3));
    graph.addEdge(edge(0, 6, 6, 3));

    graph.addEdge(edge(1, 5, 5, 1));
    graph.addEdge(edge(1, 5, 6, 1));
    graph.addEdge(edge(1, 6, 5, 1));
    graph.addEdge(edge(1, 6, 6, 1));

    graph.addEdge(edge(2, 0, 0, 0));
    graph.addEdge(edge(2, 5, 6, 1));
    graph.addEdge(edge(2, 6, 5, 1));
    graph.addEdge(edge(2, 1, 1, 2));
    graph.addEdge(edge(2, 2, 2, 2));
    graph.addEdge(edge(2, 3, 3, 2));
    graph.addEdge(edge(2, 4, 4, 2));
    graph.addEdge(edge(2, 5, 5, 2));
    graph.addEdge(edge(2, 6, 6, 2));
    graph.addEdge(edge(2, 7, 7, 2));

    graph.addEdge(edge(3, 0, 0, 0));
    graph.addEdge(edge(3, 6, 6, 0));
    graph.addEdge(edge(3, 7, 7, 0));
    graph.addEdge(edge(3, 1, 5, 1));
    graph.addEdge(edge(3, 5, 1, 1));
    graph.addEdge(edge(3, 1, 1, 2));
    graph.addEdge(edge(3, 2, 2, 2));
    graph.addEdge(edge(3, 3, 3, 2));
    graph.addEdge(edge(3, 4, 4, 2));
    graph.addEdge(edge(3, 5, 5, 2));
    graph.addEdge(edge(3, 3, 4, 4));
    graph.addEdge(edge(3, 4, 3, 4));

    graph.addEdge(edge(4, 0, 0, 0));
    graph.addEdge(edge(4, 1, 2, 1));
    graph.addEdge(edge(4, 2, 1, 1));
    graph.addEdge(edge(4, 3, 4, 1));
    graph.addEdge(edge(4, 4, 3, 1));
    graph.addEdge(edge(4, 1, 1, 4));

    return graph;
  }
}
