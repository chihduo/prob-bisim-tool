package common.encoding;

public interface ISatSolverFactory {

    ISatSolver spawnSolver();

}
