

package probsim

import java.util.Arrays

import probsim.utils.UnionFind

import scala.collection.{Set => GSet}
import scala.collection.mutable.{BitSet => MBitSet, HashMap => MHashMap,
TreeMap, LinkedHashMap, ArrayBuffer}
import scala.collection.immutable.BitSet

object ExplicitStateBisimulation {

  import FixedLengthWordOps._

  def apply(system: FixedLengthExplicitStateSystem) =
    new ExplicitStateBisimulation(system, ExplicitStateReachable(system))

  def toBitSet(bitSeqs: Iterator[(Long, Int)]): MBitSet = {
    val res = new MBitSet

    var offset = 0
    for ((value, width) <- bitSeqs) {
      var i = 0
      while (i < width) {
        if ((value & (1l << i)) != 0)
          res += offset
        i = i + 1
        offset = offset + 1
      }
    }

    res
  }

  //////////////////////////////////////////////////////////////////////////////

  type StateExtractor = (Word, Int) => Word

  val ZeroStateExtractor: StateExtractor = (_, _) => zeroWord

  def DistinctDeadEndStateExtractor
  (system: ExplicitStateSystem): StateExtractor =
    (config, len) =>
      if (system.step(config, len).hasNext)
        zeroWord
      else
        config

  def DistinctDeadEndStateExtractor2
  (system: ExplicitStateSystem): StateExtractor =
    (config, len) =>
      if (system.step(config, len).hasNext)
        zeroWord
      else {
        var p = len - 1
        var w = config
        while (p >= 0) {
          val c = ch(config, p)(system.letters)
          if (c == 3 | c == 4) {
            w = update(config, p, 0)(system.letters)
          }
          p = p - 1
        }
        w
      }

  def UnionSystemDistinctDeadEndStateExtractor
  (system: ExplicitStateSystem): StateExtractor =
    (config, len) =>
      if (system.step(config, len).hasNext)
        zeroWord
      else {
        var p = len - 1
        while (ch(config, p)(system.letters) == 0) p = p - 1
        assert(p > 0)
        update(config, p, 0)(system.letters)
      }
}

////////////////////////////////////////////////////////////////////////////////

class ExplicitStateBisimulation(system: FixedLengthExplicitStateSystem,
                                reachableStates : Iterable[FixedLengthWordOps.Word],
                                exposedStateInformation:
                                  ExplicitStateBisimulation.StateExtractor =
                                  ExplicitStateBisimulation.ZeroStateExtractor) {

  import FixedLengthWordOps._
  import ExplicitStateBisimulation.toBitSet

  private val probBitwidth = system.probBitwidth
  private val actionBitwidth = system.actions.bitwidth
  private val configBitwidth = system.wordLen * system.letters.bitwidth

  print("#states: " + reachableStates.size + "; ")

  val (bisimulation, bisimulationPartitionNum) = {
    //////////////////////////////////////////////////////////////////////////////
    // Compute initial partitions based on the probabilities of actions

    var currentPartitioning = new UnionFind[Word]

    val initialPartitionNum = {
      val probSums = new Array[Int](system.actions.size)
      val representatives = new MHashMap[MBitSet, Word]

      for (config <- reachableStates) {
        currentPartitioning makeSet config

        for ((_, p, a) <- system step config)
          probSums(a) += p

        val exposedInfo =
          exposedStateInformation(config, system.wordLen)
        val probKey =
          toBitSet((for (s <- probSums.iterator) yield (s.toLong, probBitwidth)) ++
            (Iterator single(exposedInfo, configBitwidth)))

        currentPartitioning.union(
          config, representatives.getOrElseUpdate(probKey, config))

        Arrays.fill(probSums, 0)
      }

      representatives.size
    }

    //  println("Initial equivalence classes: " + initialPartitionNum)

    //////////////////////////////////////////////////////////////////////////////
    // Fixed-point iteration to find the greatest bisimulation relation

    val representatives = new MHashMap[MBitSet, Word]
    val probSums = new TreeMap[(Word, Int), Int]

    var oldPartNum = -1
    var newPartNum = initialPartitionNum

    //    print(" refining partitions ..")
    while (oldPartNum < newPartNum) {
      print(".")
      oldPartNum = newPartNum

      val newPartitioning = new UnionFind[Word]
      for (config <- reachableStates) {
        newPartitioning makeSet config

        for ((target, p, a) <- system step config) {
          val key = (currentPartitioning(target), a)
          probSums.put(key, probSums.getOrElse(key, 0) + p)
        }

        val probKey =
          toBitSet(Iterator((currentPartitioning(config), configBitwidth)) ++
            (for (((target, a), p) <- probSums.iterator;
                  pair <- Iterator((target, configBitwidth),
                    (a.toLong, actionBitwidth),
                    (p.toLong, probBitwidth))) yield pair))

        newPartitioning.union(
          config, representatives.getOrElseUpdate(probKey, config))

        probSums.clear
      }

      newPartNum = representatives.size
      currentPartitioning = newPartitioning

      representatives.clear
    }

    (currentPartitioning, newPartNum)
  }

  println(" #classes: " + bisimulationPartitionNum)

  def printClasses: Unit = {
    val classes = new LinkedHashMap[Word, ArrayBuffer[Word]]

    for (w <- reachableStates.toArray.sorted)
      classes.getOrElseUpdate(bisimulation(w), new ArrayBuffer[Word]) += w

    implicit val _ = system.letters

    for (((_, elements), n) <- classes.iterator.zipWithIndex) {
      println
      println("Equivalence class #" + n + ":")
      for (w <- elements)
        println(pp(w, system.wordLen))
    }
  }

}

////////////////////////////////////////////////////////////////////////////////

object BisimulationBank {
  def apply(system: ExplicitStateSystem,
            exposedStateInformation: ExplicitStateBisimulation.StateExtractor) =
    new BisimulationBank(system, exposedStateInformation,
                         new ReachabilityBank(system))
}

class BisimulationBank(val system: ExplicitStateSystem,
                       val exposedStateInformation: ExplicitStateBisimulation.StateExtractor,
                       reachabilityBank : ReachabilityBank) {

  import FixedLengthWordOps._

  private val bisimulations, globalBisimulations =
    new MHashMap[Int, ExplicitStateBisimulation]

  private def explicitStateBisimulation(len: Int): ExplicitStateBisimulation =
    bisimulations.getOrElseUpdate(len, {
      print("Bisimulation for length " + len + ": ")
      new ExplicitStateBisimulation(
        ExplicitStateSystemInstance(system, len),
        reachabilityBank(len),
        exposedStateInformation)
    })

  private def globalExplicitStateBisimulation(len: Int)
                : ExplicitStateBisimulation =
    globalBisimulations.getOrElseUpdate(len, {
      print("Global bisimulation for length " + len + ": ")

      implicit val letters = system.letters

      val reachable = new ArrayBuffer[Word]
      val alphSize = letters.size

      var curWord = zeroWord
      var cont = true

      while (cont) {
        reachable += curWord

        var i = 0
        while (i < len) {
          val c = ch(curWord, i)
          val newC = (c + 1) % alphSize
          curWord = update(curWord, i, newC)
          if (newC > 0)
            i = len + 1
          else
            i = i + 1
        }

        if (i == len)
          cont = false
      }
  
      new ExplicitStateBisimulation(
        ExplicitStateSystemInstance(system, len),
        reachable,
        exposedStateInformation)
    })

  def reachableStates(len: Int): GSet[Word] =
    reachabilityBank(len)

  def reachableStatesSorted(len: Int): Seq[Word] =
    reachabilityBank sorted len

  /**
   * Bisimilar reachable states
   */
  def bisimilarStates(len: Int): UnionFind[Word] =
    explicitStateBisimulation(len).bisimulation

  /**
   * Bisimilar states globally, i.e., considering both reachable and
   * non-reachable states
   */
  def globalBisimilarStates(len: Int): UnionFind[Word] =
    globalExplicitStateBisimulation(len).bisimulation

  def printClasses(len: Int): Unit =
    bisimulations.get(len) match {
      case Some(b) => b.printClasses
      case _ => println("Bisimulation for length " + len + " is not yet computed")
    }

}
