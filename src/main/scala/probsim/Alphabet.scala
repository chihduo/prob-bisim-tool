
package probsim

import java.util.{List => JList, ArrayList, Map => JMap, HashMap => JHashMap}

import scala.collection.immutable.BitSet
import scala.collection.JavaConverters._

////////////////////////////////////////////////////////////////////////////////

trait Alphabet {
  val bitwidth : Int
  val size : Int
  def values : Range = 0 until size
  def pp(ch : Int) : String = "" + ch
  def toJMap : JMap[Integer, String] = {
    val res = new JHashMap[Integer, String]
    for (c <- values)
      res.put(c, pp(c))
    res
  }
}

case class FixedSizeAlphabet(size : Int) extends Alphabet {
  val bitwidth = {
    var w = 1
    while ((1 << w) < size)
      w = w + 1
    w
  }
}

case class NBitAlphabet(bitwidth : Int) extends Alphabet {
  val size = 1 << bitwidth
}

class LabelAlphabet[E <: Enumeration](val labels : E) extends Alphabet {
  val size = labels.maxId

  val bitwidth = {
    var w = 1
    while ((1 << w) < size)
      w = w + 1
    w
  }

  def int2label(ch : Int) : labels.Value = labels(ch)

  override def pp(ch : Int) : String = int2label(ch).toString
}

////////////////////////////////////////////////////////////////////////////////

object LetterAlphabet {
  case class WrappedLetter(value : Int)
}

case class LetterAlphabet[E <: Enumeration](_labels : E)
           extends LabelAlphabet[E](_labels) {
  implicit def label2Int(l : labels.Value) : Int = l.id

  import LetterAlphabet.WrappedLetter
  implicit def wrapLetter(l : labels.Value) : WrappedLetter =
    WrappedLetter(l.id)

  implicit def letter2Pattern(l : labels.Value) : Rewriting.Pattern =
    Rewriting.CharPattern(label2Int(l))
}

////////////////////////////////////////////////////////////////////////////////

object ActionAlphabet {
  case class WrappedAction(value : Int)
}

case class ActionAlphabet[E <: Enumeration](_labels : E)
           extends LabelAlphabet[E](_labels) {
  import ActionAlphabet.WrappedAction
  implicit def wrapAction(l : labels.Value) : WrappedAction =
    WrappedAction(l.id)
}

////////////////////////////////////////////////////////////////////////////////

object UnitActions extends Enumeration {
  val A = Value
}

object UnitActionAlphabet extends ActionAlphabet(UnitActions)

////////////////////////////////////////////////////////////////////////////////

object FixedLengthWordOps {

  type Word = Long
  
  def zeroWord : Word = 0

  def ch(w : Word, pos : Int)
        (implicit alph : Alphabet) : Int = {
    val bw = alph.bitwidth
    (w >>> (pos * bw)).toInt & ((1 << bw) - 1)
  }

  def update(w : Word, pos : Int, ch : Int)
            (implicit alph : Alphabet) : Word = {
    val bw = alph.bitwidth
    ((((w >>> ((pos + 1) * bw)) << bw) + ch.toLong) << (pos * bw)) +
    (w & ((1l << (pos * bw)) - 1l))
  }

  def update(w : Word, pos : Int, replacement : Seq[Int])
            (implicit alph : Alphabet) : Word = {
    var res = w
    for ((ch, i) <- replacement.iterator.zipWithIndex)
      res = update(res, pos + i, ch)
    res
  }

  def repeatLetters(num : Int)
                   (letters : LetterAlphabet.WrappedLetter*)
                   (implicit alph : Alphabet) : Seq[LetterAlphabet.WrappedLetter] =
    for (_ <- 0 until num; l <- letters) yield l

  def sequence(letters : LetterAlphabet.WrappedLetter*)
              (implicit alph : Alphabet) : Word =
    multSequence((for (l <- letters) yield (l, 1)) : _*)

  def multSequence(letters : (LetterAlphabet.WrappedLetter, Int)*)
                  (implicit alph : Alphabet) : Word = {
    var res : Word = zeroWord
    var offset = 0
    for ((LetterAlphabet.WrappedLetter(letter), num) <- letters.iterator;
         oldOffset = {
           val o = offset
           offset = offset + num
           o
         };
         i <- oldOffset until offset)
      res = update(res, i, letter)
    res
  }

  def toWord(letters : Int*)(implicit alph : Alphabet) : Word =
    (letters :\ 0l) { case (l, w) => l + (w << alph.bitwidth) }

  def toBitSec(letters : Int*)(implicit alph : Alphabet) : BitSet = {
    null
  }

  def toLabelSeq[E <: Enumeration]
                (w : Word, len : Int)
                (implicit alph : LabelAlphabet[E])
                : Seq[alph.labels.Value] =
    (for (i <- 0 until len) yield alph.int2label(ch(w, i))).toVector

  def toSeq(w : Word, len : Int)
           (implicit alph : Alphabet) : Seq[Int] =
    (for (i <- 0 until len) yield ch(w, i)).toVector

  def transcode(w : Word, len : Int)
               (fromAlph : Alphabet, toAlph : Alphabet) : Word =
    if (fromAlph.size == toAlph.size)
      w
    else
      toWord((for (pos <- 0 until len) yield ch(w, pos)(fromAlph)) : _*)(toAlph)

  def pp(w : Word, len : Int)
        (implicit alph : Alphabet) : String = alph match {
    case alph : LabelAlphabet[_] => {
      implicit val _ = alph
      "[" + (toLabelSeq(w, len) mkString ", ") + "]"
    }
    case _ =>
      "[" + (toSeq(w, len) mkString ", ") + "]"
  }

  def jlist2Word(word: JList[Integer])
                (implicit alph : Alphabet): (Word, Int) = {
    val len = word.size
    assert(len * alph.bitwidth <= 64)

    val it = word.iterator
    var n = 0
    var res : Word = zeroWord

    while (it.hasNext) {
      res = update(res, n, it.next.toInt)
      n = n + 1
    }

    (res, len)
  }

  def word2JList(w : Word, len : Int)
                (implicit alph : Alphabet) : JList[Integer] = {
    val res = new ArrayList[Integer]
    for (n <- 0 until len)
      res add ch(w, n)
    res
  }

  //////////////////////////////////////////////////////////////////////////////

  def encodeLetterPair(x: Int, y: Int)
                      (implicit alph : Alphabet) : Int =
    x * alph.size + y

  def decodeLetterPair(x: Int)
                      (implicit alph : Alphabet): (Int, Int) =
    (x / alph.size, x % alph.size)

  def decodeWordPair(word: JList[Integer])
                    (implicit alph : Alphabet): (Word, Word, Int) = {
    val len = word.size
    assert(len * alph.bitwidth <= 64)
    val (w1, w2) =
      (for (l <- word.asScala) yield decodeLetterPair(l.toInt)).unzip
    (toWord(w1: _*), toWord(w2: _*), len)
  }

  def encodeWordPair(w1: Word, w2: Word, len: Int)
                    (implicit alph : Alphabet): JList[Integer] = {
    val res = new ArrayList[Integer]
    for (n <- 0 until len)
      res add encodeLetterPair(ch(w1, n), ch(w2, n))
    res
  }

}

