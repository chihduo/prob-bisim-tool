
package probsim.sat

import common.VerificationUltility
import common.bellmanford.DirectedEdgeWithInputOutput
import probsim._
import probsim.utils.{UnionFind, ProofProdUnionFind}
import common.finiteautomata.{Automata, AutomataConverter}
import common.bellmanford.EdgeWeightedDigraph
import common.encoding._
import common.elimination._

import scala.collection.JavaConverters._
import scala.collection.mutable.{TreeMap, HashSet => MHashSet}
import scala.util.control.Breaks

import java.util.{List => JList, ArrayList, HashSet => JHashSet}


abstract class AbstractNStateBisimulation(
                         stateNum : Int,
                         system : ExplicitStateSystem,
                         lowerBisimBound : Int=>
                                 Seq[(FixedLengthWordOps.Word,
                                      FixedLengthWordOps.Word)],
                         bisimulations : BisimulationBank) {
  import FixedLengthWordOps._

  implicit val letterAlphabet = system.letters
  val alphSize = letterAlphabet.size

  def decodeWord(word: JList[Integer]): (Word, Int) = jlist2Word(word)

  def encodeWord(w : Word, len : Int) : JList[Integer] = word2JList(w, len)

  val solver : ISatSolver = new SatSolver
  val encoding = new TransducerEncoding(solver, stateNum, alphSize)
  encoding.encode

  def allocateSatVars(n : Int) : IndexedSeq[Int] = {
    val v = solver.getNextSATVar
    solver.setNextSATVar(v + n)
    (v until (v + n)).toVector
  }

  def addClause(vars : Int*) : Unit =
    solver addClause vars.toArray

  //////////////////////////////////////////////////////////////////////////////

  val acceptanceTree = new PairAcceptanceTree (encoding)

  def addPairSymm(w1 : Word, w2 : Word, len : Int) : Int = {
    addPair(w1, w2, len)
    addPair(w2, w1, len)
  }

  def addPair(w1 : Word, w2 : Word, len : Int) : Int = {
    val wl1 = encodeWord(w1, len)
    val wl2 = encodeWord(w2, len)

    val res = acceptanceTree.insert(wl1, wl2)

    acceptanceTree.contains(wl2, wl1) match {
      case 0 =>
      // nothing
      case w => {
        // directly add constraints about symmetry
        addClause(-res, w)
        addClause(-w, res)
      }
    }

    res
  }

  //////////////////////////////////////////////////////////////////////////////

  def encodeGlobalReflexivity = {
    // flags that indicate that a particular state is used to ensure
    // reflexitivity
    val rFlags = allocateSatVars(stateNum)

    // initial state is one of those
    addClause(rFlags(0))

    // selected states have to be accepting
    for (s <- 0 until stateNum)
      addClause(-rFlags(s), encoding getIndexZVar (s + 1))

    // from every selected states , and for any letter l, there have to be
    // transitions with label (l, l) to another selected state
    for (s <- 0 until stateNum; l <- 0 until alphSize) {
      val nextVars = allocateSatVars(stateNum)
      addClause((List(-rFlags(s)) ++ nextVars) : _*)
      for (t <- 0 until stateNum) {
        addClause(-nextVars(t), rFlags(t))
        addClause(-nextVars(t), encoding.getTransBoolVar(s + 1, l, l, t + 1))
      }
    }
  }

  //////////////////////////////////////////////////////////////////////////////

  def encodeGlobalSymmetry = {
    // flags indicating whether a pair of states (s1, s2) can be reached
    // with symmetric word pairs (a, b) and (b, a), respectively
    val rFlags =
      (for (s <- 0 until stateNum) yield allocateSatVars(s+1)).toVector

    // flags indicating that states must not be accepting, because they would
    // otherwise violate symmetry

    // NB: those are not actually needed, because we assume that an accepting
    // state can be reached from all states

//    val naFlags =
//      allocateSatVars(stateNum)

    // (init, init) is reachable
    addClause(rFlags(0)(0))

    // preservation under transitions
    for (s1 <- 0 until stateNum; s2 <- 0 to s1;
         t1 <- 0 until stateNum; t2 <- 0 to t1;
         l1 <- 0 until alphSize; l2 <- 0 until alphSize) {
      addClause(-rFlags(s1)(s2),
                -encoding.getTransBoolVar(s1+1, l1, l2, t1+1),
                -encoding.getTransBoolVar(s2+1, l2, l1, t2+1),
                rFlags(t1)(t2))
      addClause(-rFlags(s1)(s2),
                -encoding.getTransBoolVar(s1+1, l1, l2, t2+1),
                -encoding.getTransBoolVar(s2+1, l2, l1, t1+1),
                rFlags(t1)(t2))
    }

/*
    for (s1 <- 0 until stateNum;
         t1 <- 0 until stateNum;
         l1 <- 0 until alphSize; l2 <- 0 until alphSize)
      addClause(-naFlags(s1),
                -encoding.getTransBoolVar(s1+1, l1, l2, t1+1),
                naFlags(t1))
 */

    // if one of the paired states has an outgoing transition for (l1, l2),
    // then the other one also needs one for (l2, l1)
    for (s1 <- 0 until stateNum; s2 <- 0 to s1;
         t1 <- 0 until stateNum;
         l1 <- 0 until alphSize; l2 <- 0 until alphSize) {
      addClause(List(-rFlags(s1)(s2),
                     -encoding.getTransBoolVar(s1+1, l1, l2, t1+1)) ++
                (for (t2 <- 0 until stateNum)
                 yield encoding.getTransBoolVar(s2+1, l2, l1, t2+1)) : _*)
      addClause(List(-rFlags(s1)(s2),
                     -encoding.getTransBoolVar(s2+1, l1, l2, t1+1)) ++
                (for (t2 <- 0 until stateNum)
                 yield encoding.getTransBoolVar(s1+1, l2, l1, t2+1)) : _*)
    }

    // equal acceptance
    for (s1 <- 0 until stateNum; s2 <- 0 until s1) {
      addClause(-rFlags(s1)(s2),
                -encoding.getIndexZVar(s1+1), encoding.getIndexZVar(s2+1))
      addClause(-rFlags(s1)(s2),
                -encoding.getIndexZVar(s2+1), encoding.getIndexZVar(s1+1))
    }

    // non-acceptance
//    for (s1 <- 0 until stateNum)
//      addClause(-naFlags(s1), -encoding.getIndexZVar(s1+1))
  }

  //////////////////////////////////////////////////////////////////////////////

  def checkLocalTransitivityUF(candidate : EdgeWeightedDigraph) = {
    val setVars = solver.positiveModelVars
    val rel = new ProofProdUnionFind[JList[Integer]]

    acceptanceTree.visit(new PairAcceptanceTreeVisitor {
      def visit(a : JList[Integer], b : JList[Integer], abAccept : Int) = {
        val ac = PairAcceptanceTree cloneList a
        val bc = PairAcceptanceTree cloneList b
        if (!(rel contains ac))
          rel makeSet ac
        if (!(rel contains bc))
          rel makeSet bc
        if (setVars contains abAccept)
          rel.union(ac, bc)
      }
    })

    acceptanceTree.visit(new PairAcceptanceTreeVisitor {
      def visit(a : JList[Integer], b : JList[Integer], abAccept : Int) = {
        if (!(setVars contains abAccept) && rel(a) == rel(b)) {
          val path = rel.getPath(a, b)
          val pathWords = for (c <- path) yield decodeWord(c)._1
          val len = path.head.size

          println("E1: Local transitivity:         " +
                  pp(pathWords.head, len) + ", " + pp(pathWords.last, len))

          addClause((for (Seq(x, y) <- pathWords sliding 2)
                     yield -addPair(x, y, len)).toList ++ List(abAccept) : _*)
          throw new FoundCounterexample(0)
        }
      }
    })
  }

  //////////////////////////////////////////////////////////////////////////////

  case class FoundCounterexample(size : Int) extends RuntimeException

  // check that the relation is a bisimulation relation
  def checkBisimProp(encodedEqv : UnionFind[Word],
                     eqvClasses : Word => Iterable[Word],
                     len : Int,
                     consideredConfigs : Iterable[Word]) {
        for (repres <- consideredConfigs)
          if ((encodedEqv contains repres) && encodedEqv(repres) == repres) {
            // first compute the outgoing probabilities of the representative
            val reprSums = computeProbSums(repres, len, encodedEqv)

            for (w <- eqvClasses(repres))
              if (w != repres) {
                val wSums = computeProbSums(w, len, encodedEqv)
                if (reprSums != wSums) {
                  println("E: Bisimulation not preserved:  " +
                          pp(repres, len) + ", " + pp(w, len))

                  for (a <- 0 until system.actions.size) {
                    val f : ((Word, Int)) => Boolean = (p) => p._2 == a
                    if ((reprSums filterKeys f) != (wSums filterKeys f)) {
                      val targets =
                        (for ((target, p, `a`) <-
                              system.step(repres, len) ++ system.step(w, len))
                         yield target).toVector.distinct

                      val targetVars =
                        for (i <- 0 until targets.size;
                             iw = targets(i);
                             j <- (i + 1) until targets.size;
                             jw = targets(j);
                             if (encodedEqv repr iw) != (encodedEqv repr jw))
                        yield addPair(iw, jw, len)

                      solver addClause (targetVars ++
                                        List(-addPair(repres, w, len))).toArray

                      throw new FoundCounterexample(len)
                    }
                  }

                  assert(false) // should not get to this point
                }
              }
          }
  }


  // check that the relation is a bisimulation relation
  def checkReachableBisimProp(encodedEqv : UnionFind[Word],
                              eqvClasses : Word => Iterable[Word],
                              len : Int) =
    checkBisimProp(encodedEqv, eqvClasses, len,
                   bisimulations reachableStatesSorted len)

  def computeProbSums(
                w : Word, len : Int,
                encodedEqv : UnionFind[Word]) : TreeMap[(Word, Int), Int] = {
    val res = new TreeMap[(Word, Int), Int]

    for ((target, p, a) <- system.step(w, len)) {
      val key = (encodedEqv repr target, a)
      res.put(key, res.getOrElse(key, 0) + p)
    }

    res
  }

  //////////////////////////////////////////////////////////////////////////////

  def transducer2UnionFind(
         transducer : EdgeWeightedDigraph,
         wordLen : Int) : UnionFind[Word] = {
    val res = new UnionFind[Word] // new ProofProdUnionFind[Word]
    val allWords = new MHashSet[Word]

    AutomataConverter.visitWordPairs(
      transducer,
      wordLen,
      new AutomataConverter.WordPairVisitor {
        def apply(wl1 : JList[Integer], wl2 : JList[Integer]) : Boolean = {
          val (w1, _) = decodeWord(wl1)
          val (w2, _) = decodeWord(wl2)

          if (allWords add w1)
            res makeSet w1
          if (allWords add w2)
            res makeSet w2

          res.union(w1, w2)

          false
        }
      });

    res
  }

  def transducer2UnionFindReachable(
         transducer : EdgeWeightedDigraph,
         wordLen : Int) : ProofProdUnionFind[Word] = {
    val res = new ProofProdUnionFind[Word]
    val reachable = bisimulations reachableStatesSorted wordLen

    for (w <- reachable)
      res makeSet w

    val encodedWords =
      for (w <- reachable) yield (w, encodeWord(w, wordLen))

    for ((w1, wl1) <- encodedWords)
      for ((w2, wl2) <- encodedWords)
        if (transducer.accepts(wl1, wl2))
          res.union(w1, w2)

/*
    AutomataConverter.visitWordPairs(
      transducer,
      wordLen,
      new AutomataConverter.WordPairVisitor {
        def apply(wl1 : JList[Integer], wl2 : JList[Integer]) : Boolean = {
          val (w1, _) = decodeWord(wl1)
          val (w2, _) = decodeWord(wl2)
          if (reachable(w1) && reachable(w2))
            res.union(w1, w2)
          false
        }
      });
 */
    res
  }

  //////////////////////////////////////////////////////////////////////////////

  def transducer2Aut(transducer : EdgeWeightedDigraph) : Automata = {
    val res = new Automata (transducer.getInitState,
                            transducer.V,
                            alphSize * alphSize)
    for (edge <- transducer.edges.asScala) {
      val ioEdge = edge.asInstanceOf[DirectedEdgeWithInputOutput]
      res.addTrans(edge.from,
                   encodeLetterPair(ioEdge.getInput, ioEdge.getOutput),
                   edge.to)
    }
    res setAcceptingStates transducer.getAcceptingStates
    res
  }

  def aut2transducer(aut : Automata) : EdgeWeightedDigraph = {
    val states = aut.getStates
    val res = new EdgeWeightedDigraph(states.size, aut.getInitState,
                                      aut.getAcceptingStates)

    for (s <- states;
         p <- s.getOutgoingLabels.asScala;
         t <- s.getDest(p).asScala) {
      val (l1, l2) = decodeLetterPair(p)
      res.addEdge(new DirectedEdgeWithInputOutput(s.getId, t, l1, l2))
    }
    res
  }

  /**
   * Automaton defining strict lexicographic order < on words of equal length
   */
  def lexOrdering : Automata = {
    val res = new Automata(0, 3, alphSize * alphSize)

    val accepting = new JHashSet[Integer]
    accepting add 2
    res setAcceptingStates accepting

    for (l <- letterAlphabet.values)
      res.addTrans(0, encodeLetterPair(l, l), 0)

    for (l1 <- letterAlphabet.values; l2 <- (l1 + 1) until alphSize) {
      res.addTrans(0, encodeLetterPair(l1, l2), 2)
      res.addTrans(0, encodeLetterPair(l2, l1), 1)
    }

    for (l1 <- letterAlphabet.values; l2 <- letterAlphabet.values) {
      res.addTrans(1, encodeLetterPair(l1, l2), 1)
      res.addTrans(2, encodeLetterPair(l1, l2), 2)
    }

    res
  }

  def transducer2UFForest(transducer : EdgeWeightedDigraph)
                          : EdgeWeightedDigraph = {
    val oriAut = transducer2Aut(transducer)
    val orderedAut = VerificationUltility.getIntersection(oriAut, lexOrdering)

    // project to right component
    val rightProj = {
      val states = orderedAut.getStates
      val res = new Automata(orderedAut.getInitState, states.size, alphSize)
      res setAcceptingStates orderedAut.getAcceptingStates

      for (s <- states;
           p <- s.getOutgoingLabels.asScala;
           t <- s.getDest(p).asScala) {
        val (_, l2) = decodeLetterPair(p)
        res.addTrans(s.getId, l2, t)
      }

      AutomataConverter minimise res
    }

    val nonRightProj = AutomataConverter getComplement rightProj

    // back to pairs, where the left component does not occur as any right
    // component of the orderedAut
    val nonRight = {
      val states = nonRightProj.getStates
      val res = new Automata(nonRightProj.getInitState, states.size,
                             alphSize * alphSize)
      res setAcceptingStates nonRightProj.getAcceptingStates

      for (s <- states;
           l1 <- s.getOutgoingLabels.asScala;
           t <- s.getDest(l1).asScala;
           l2 <- letterAlphabet.values)
        res.addTrans(s.getId, encodeLetterPair(l1, l2), t)

      res
    }

    val resAut = VerificationUltility.getIntersection(orderedAut, nonRight)
    val res = aut2transducer(resAut)

//    println(res)

    res
  }

}
