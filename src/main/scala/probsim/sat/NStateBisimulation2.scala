
package probsim.sat

import probsim._
import probsim.utils.{ProofProdUnionFind, UnionFind}
import common.finiteautomata.{AutomataConverter, MonaUtility}
import common.bellmanford.EdgeWeightedDigraph
import common.encoding._
import common.elimination._

import scala.collection.JavaConverters._
import scala.collection.mutable.TreeMap
import scala.util.control.Breaks
import java.util.{ArrayList, List => JList}

object NStateBisimulation2 {

}

class NStateBisimulation2(stateNum: Int,
                          system: ExplicitStateSystem,
                          lowerBisimBound: Int => Seq[(FixedLengthWordOps.Word,
                            FixedLengthWordOps.Word)],
                          bisimulations: BisimulationBank) {

  import FixedLengthWordOps._

  private implicit val letterAlphabet = system.letters
  val alphSize = letterAlphabet.size

  var maxCheckLen = 8

  def decodeWord(word: JList[Integer]): (Word, Int) = jlist2Word(word)

  def encodeWord(w : Word, len : Int) : JList[Integer] = word2JList(w, len)

  def encodeLetterPair(x: Int, y: Int): Int =
    x * alphSize + y

  def decodeLetterPair(x: Int): (Int, Int) =
    (x / alphSize, x % alphSize)

  def decodeWordPair(word: JList[Integer]): (Word, Word, Int) = {
    val len = word.size
    assert(len * letterAlphabet.bitwidth <= 64)
    val (w1, w2) =
      (for (l <- word.asScala) yield decodeLetterPair(l.toInt)).unzip
    (toWord(w1: _*), toWord(w2: _*), len)
  }

  //////////////////////////////////////////////////////////////////////////////

  val solver: ISatSolver = new SatSolver
  val encoding = new TransducerEncoding(solver, stateNum, system.letters.size)
  encoding.encode

  val acceptanceTree = new PairAcceptanceTree(encoding)

  def addPairSymm(w1: Word, w2: Word, len: Int): Int = {
    addPair(w1, w2, len)
    addPair(w2, w1, len)
  }

  def addPair(w1: Word, w2: Word, len: Int): Int = {
    val wl1 = encodeWord(w1, len)
    val wl2 = encodeWord(w2, len)

    val res = acceptanceTree.insert(wl1, wl2)

    acceptanceTree.contains(wl2, wl1) match {
      case 0 =>
      // nothing
      case w => {
        // directly add constraints about symmetry
        solver addClause Array(-res, w)
        solver addClause Array(-w, res)
      }
    }

    res
  }

  //////////////////////////////////////////////////////////////////////////////

  val result: Option[EdgeWeightedDigraph] = {
    val breaks = new Breaks
    import breaks.{breakable, break}

    var res: Option[EdgeWeightedDigraph] = None
    var n = 0
    while (res.isEmpty && solver.isSatisfiable && n < 200) breakable {
      print("#" + n + "\t")
      n = n + 1

      val candidate =
        BoolValToAutomaton.toTransducer(solver.positiveModelVars, encoding)
      //      println(candidate)

      for (len <- 0 to maxCheckLen) {
        val reachable = bisimulations reachableStates len
        val bisim = bisimulations bisimilarStates len

        val encodedEqv = transducer2UnionFind(candidate, len)

        // check that we are above the lower bound
        for ((w1, w2) <- lowerBisimBound(len))
          if (encodedEqv(w1) != encodedEqv(w2)) {
            println("A: Words should be equivalent: " +
              pp(w1, len) + ", " + pp(w2, len))
            solver addClause Array(addPair(w1, w2, len))
            break
          }

        // check that the computed relation includes the identity relation
        val reachableSorted = reachable.toArray.sorted
        for (w <- reachableSorted) {
          val wl = encodeWord(w, len)
          if (!candidate.accepts(wl, wl)) {
            println("B: Reflexitivity violated:      " + pp(w, len))
            solver addClause Array(addPair(w, w, len))
            break
          }
        }

        // check that the computed relation is an equivalence relation
        val eqvClasses = encodedEqv.classes
        for (w1 <- reachableSorted) {
          val wl1 = encodeWord(w1, len)
          for (w2 <- eqvClasses(encodedEqv(w1))) {
            val wl2 = encodeWord(w2, len)
            if (!candidate.accepts(wl1, wl2)) {
              println("C: Equivalence axioms violated: " +
                pp(w1, len) + ", " + pp(w2, len))
              val path = encodedEqv.getPath(w1, w2)
              val steps =
                (for (Seq(a, b) <- path sliding 2)
                  yield -addPairSymm(a, b, len)).toList
              if (path.size > 2) {
                val bigStep = addPairSymm(path.head, path.last, len)
                solver addClause (steps ++ List(bigStep)).toArray
              }
              break
            }
          }
        }

        // check that no two states are related that are not bisimilar
        for (w1 <- reachableSorted)
          for (w2 <- eqvClasses(encodedEqv(w1)))
            if (bisim(w1) != bisim(w2)) {
              println("D: Words not bisimilar:         " +
                pp(w1, len) + ", " + pp(w2, len))
              solver addClause Array(-addPair(w1, w2, len))
              break
            }

        // check that the relation is a bisimulation relation
        for (repr <- reachableSorted)
          if (encodedEqv(repr) == repr) {
            // first compute the outgoing probabilities of the representative
            val reprSums = computeProbSums(repr, len, encodedEqv)

            for (w <- eqvClasses(repr))
              if (w != repr) {
                val wSums = computeProbSums(w, len, encodedEqv)
                if (reprSums != wSums) {
                  println("E: Bisimulation not preserved:  " +
                    pp(repr, len) + ", " + pp(w, len))

                  for (a <- 0 until system.actions.size) {
                    val f: ((Word, Int)) => Boolean = (p) => p._2 == a
                    if ((reprSums filterKeys f) != (wSums filterKeys f)) {
                      val targets =
                        (for ((target, p, `a`) <-
                              system.step(repr, len) ++ system.step(w, len))
                          yield target).toVector.distinct

                      val targetVars =
                        for (i <- 0 until targets.size;
                             iw = targets(i);
                             j <- (i + 1) until targets.size;
                             jw = targets(j);
                             if encodedEqv(iw) != encodedEqv(jw))
                          yield addPair(iw, jw, len)

                      solver addClause (targetVars ++
                        List(-addPair(repr, w, len))).toArray

                      break
                    }
                  }

                  break
                }
              }
          }

      }

      println("All check succeeded")
      println
      println(candidate)

      MonaUtility.writeMonaDFA("test.dfa", candidate,
        letterAlphabet.bitwidth, true, false)
      val cex = new java.util.ArrayList[Integer]()
      if (!MonaUtility.getCexFromTeacher("test.mona",
        letterAlphabet.bitwidth, cex, true)) {
        throw new Exception("Something wrong with Mona")
      }
      if (cex.size() == 0) {
        println("=== The hypothesis passes the Mona test! ===\n")
        res = Some(candidate)
      } else {
        val (w1, w2, len) = decodeWordPair(cex)
        val cexMsg = "(" + pp(w1, len)(system.letters) +
          ", " + pp(w2, len)(system.letters) + ")"
        println("Mona gives a counterexample: " + cexMsg)
        //val u1 = encodeWord(w1, len)
        //val u2 = encodeWord(w2, len)
        //val acc = candidate.accepts(u1, u2)
        maxCheckLen = maxCheckLen + 2
      }
    }
    if (res.isEmpty)
      println("No solution found!")
    res
  }

  private def computeProbSums(w: Word, len: Int,
                              encodedEqv: UnionFind[Word]): TreeMap[(Word, Int), Int] = {
    val res = new TreeMap[(Word, Int), Int]
    for ((target, p, a) <- system.step(w, len)) {
      val key = (encodedEqv(target), a)
      res.put(key, res.getOrElse(key, 0) + p)
    }
    res
  }

  //////////////////////////////////////////////////////////////////////////////

  def transducer2UnionFind(transducer: EdgeWeightedDigraph,
                           wordLen: Int): ProofProdUnionFind[Word] = {
    val res = new ProofProdUnionFind[Word]
    val reachable = bisimulations reachableStates wordLen

    for (w <- reachable)
      res makeSet w

    AutomataConverter.visitWordPairs(
      transducer,
      wordLen,
      new AutomataConverter.WordPairVisitor {
        def apply(wl1: JList[Integer], wl2: JList[Integer]): Boolean = {
          val (w1, _) = decodeWord(wl1)
          val (w2, _) = decodeWord(wl2)
          if (reachable(w1) && reachable(w2))
            res.union(w1, w2)
          false
        }
      });

    res
  }

}
