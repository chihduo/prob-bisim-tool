
package probsim.sat

import probsim._
import probsim.utils.{UnionFind, ProofProdUnionFind}
import common.finiteautomata.AutomataConverter
import common.bellmanford.{EdgeWeightedDigraph, DirectedEdgeWithInputOutput}
import common.encoding._
import common.elimination._
import common.finiteautomata.MonaUtility

import scala.collection.JavaConverters._
import scala.collection.mutable.TreeMap
import scala.util.control.Breaks

import java.util.{List => JList, ArrayList}


object GlobalNStateBisimulation {

  type LowerBisimBound =
    Int => Seq[(FixedLengthWordOps.Word, FixedLengthWordOps.Word)]

  def apply(stateNums : Range,
            system : ExplicitStateSystem,
            prefix: String,
            lowerBisimBound : LowerBisimBound,
            bisimulations : BisimulationBank,
            initialCheckLen : Int = 9,
            verificationThreshold : Int = 2)
          : Option[GlobalNStateBisimulation] = {
    val it =
      for (n <- stateNums.iterator;
           b = new GlobalNStateBisimulation(n, system, prefix, lowerBisimBound,
                                            bisimulations, initialCheckLen,
                                            verificationThreshold);
           if b.result.isDefined)
      yield b
    if (it.hasNext)
      Some(it.next)
    else
      None
  }

}


class GlobalNStateBisimulation(
                         stateNum : Int,
                         system : ExplicitStateSystem,
                         prefix: String,
                         lowerBisimBound : GlobalNStateBisimulation.LowerBisimBound,
                         bisimulations : BisimulationBank,
                         initialCheckLen : Int = 9,
                         verificationThreshold : Int = 2)
      extends AbstractNStateBisimulation(stateNum, system,
                                         lowerBisimBound, bisimulations) {
  import FixedLengthWordOps._

  println
  println("Computing bisimulation with " + stateNum + " states")

  private val transitivitySet =
    new TransitivityPairSet (encoding, acceptanceTree)

  def fixTransitivity(cex : JList[JList[Integer]]) = {
    val len = cex.get(0).size
    val decoded = for (c <- cex.asScala) yield decodeWord(c)._1

    addClause((for (Seq(w1, w2) <- decoded sliding 2)
               yield -addPair(w1, w2, len)).toList ++
              List(addPair(decoded.head, decoded.last, len)) : _*)

/*
    for (i <- 0 until (cex.size - 1))
      transitivitySet.addPair(cex.get(i), cex.get(i+1))

    if (cex.size == 3)
      transitivitySet.addPair(cex.get(0), cex.get(2));
    else
      transitivitySet.fixTransitivityCEX(cex);
 */
  }

  def checkTransitivity(candidate : EdgeWeightedDigraph) = {
    val checking = new TransitivityChecking(candidate, alphSize)
    val cex = checking.check

    if (cex != null) {
      val (w1, len) = decodeWord(cex.get(0))
      val (w2, _)   = decodeWord(cex.get(cex.size - 1))
      println("E2: Transitivity violated:      " +
              pp(w1, len) + ", " + pp(w2, len))
      fixTransitivity(cex)
      throw new FoundCounterexample(0)
    }
  }

  def chooseBisimulations(len : Int) : UnionFind[Word] =
    // only explicitly compute reachable bisimilar configurations
    // when len gets too big
    if (len > 6)
      bisimulations bisimilarStates len
    else
      bisimulations globalBisimilarStates len

  val exposedStateInformation = bisimulations.exposedStateInformation

  def definitelyNonBisimilar(w1 : Word, w2 : Word, len : Int) : Boolean =
    exposedStateInformation(w1, len) != exposedStateInformation(w2, len) || {
      val bisim = chooseBisimulations(len)
      (bisim contains w1) && (bisim contains w2) && bisim(w1) != bisim(w2)
    }

  override def addPair(w1 : Word, w2 : Word, len : Int) : Int = {
    val wl1 = encodeWord(w1, len)
    val wl2 = encodeWord(w2, len)
    acceptanceTree.contains(wl1, wl2) match {
      case 0 =>
        acceptanceTree.contains(wl2, wl1) match {
          case 0 => {
            val v = transitivitySet.addPair(wl1, wl2)

            var cont = true
            while (cont) {
              val (w1, len) = decodeWord(wl1)
              val (w2, _)   = decodeWord(wl2)

              if (definitelyNonBisimilar(w1, w2, len))
                addClause(-acceptanceTree.contains(wl1, wl2))

              if (!wl1.isEmpty) {
                wl1.remove(len - 1)
                wl2.remove(len - 1)
              } else {
                cont = false
              }
            }

            v
          }
          case v => v
        }
      case v => v
    }
  }

  ///////////////////////////////////////////////////////////////////////////// 

  def checkSymmetry(candidate : EdgeWeightedDigraph) = {
    // invert the given relation
    val inverted = new EdgeWeightedDigraph(candidate.V)

    for (edge <- candidate.edges.asScala) {
      val ioEdge = edge.asInstanceOf[DirectedEdgeWithInputOutput]
      inverted addEdge (new DirectedEdgeWithInputOutput (edge.from, edge.to,
                                                         ioEdge.getOutput,
                                                         ioEdge.getInput))
    }

    inverted setInitState candidate.getInitState
    inverted setAcceptingStates candidate.getAcceptingStates

    L3TransducerInclusionChecking.findShortestCounterExample(
                                    inverted, candidate) match {
      case null =>
        // nothing
      case cex => {
        val (wl1, wl2) = (for (a <- cex.asScala) yield (a(0), a(1))).unzip

        val len = cex.size
        val w1 = toWord(wl1 : _*)
        val w2 = toWord(wl2 : _*)

        println("E3: Symmetry violated: "
                + pp(w1, len) + ", " + pp(w2, len))

        addPairSymm(w1, w2, len)
        throw new FoundCounterexample(0)
      }
    }
  }

  def checkLocalTransitivity(candidate : EdgeWeightedDigraph) = {
    val cex = transitivitySet transitivityCEXes solver.positiveModelVars
    if (!cex.isEmpty) {
      println("E1: Local transitivity violated")
      for (l <- cex.asScala)
        fixTransitivity(l)
      throw new FoundCounterexample(0)
    }
  }

  encodeGlobalReflexivity
  encodeGlobalSymmetry

  //////////////////////////////////////////////////////////////////////////////

  val result : Option[EdgeWeightedDigraph] = {
    var largestCEXSize : Int = initialCheckLen - verificationThreshold
    var res : Option[EdgeWeightedDigraph] = None

    var n = 1
    while (res.isEmpty && solver.isSatisfiable) try {
      print("#" + n + "\t")
      n = n + 1

      val candidate =
        BoolValToAutomaton.toTransducer(solver.positiveModelVars, encoding)
//      println(candidate)

      checkLocalTransitivityUF(candidate)
//      checkLocalTransitivity(candidate)
//      checkSymmetry(candidate)
      checkTransitivity(candidate)

      val forestTransducer = transducer2UFForest(candidate)

      for (len <- 0 to (largestCEXSize + verificationThreshold)) {

        // check that we are above the lower bound
        for ((w1, w2) <- lowerBisimBound(len))
          if (!candidate.accepts(encodeWord(w1, len), encodeWord(w2, len))) {
            println("B1: Words should be equivalent: " +
                    pp(w1, len) + ", " + pp(w2, len))
            addClause(addPair(w1, w2, len))
            throw new FoundCounterexample(len)
          }

        val encodedEqv = transducer2UnionFind(forestTransducer, len)
        val eqvClasses = encodedEqv.classes
        val bisim = chooseBisimulations(len)
        
        // check that no two states are related that are definitely not
        // bisimilar
        for (w1 <- encodedEqv.elements)
          for (w2 <- eqvClasses(encodedEqv(w1)))
            if (definitelyNonBisimilar(w1, w2, len)) {
              println("B2: Words not bisimilar:        " +
                      pp(w1, len) + ", " + pp(w2, len))
              addClause(-addPair(w1, w2, len))
              throw new FoundCounterexample(len)
            }

        // check that the relation is a bisimulation relation
        checkBisimProp(encodedEqv, eqvClasses, len,
                       encodedEqv.representatives.toVector.sorted)

        if (len >= 6) {
          println("All checks up to length " + len + " succeeded")
          println(candidate)
        }
      }

      println("All check succeeded")
      println
      println(candidate.prettyPrint("Probabilistic_bisimulation",
                                    letterAlphabet.toJMap,
                                    letterAlphabet.toJMap))

      doMonaCheck(candidate)

      res = Some(candidate)
    } catch {
      case FoundCounterexample(len) => {
        largestCEXSize = largestCEXSize max len
//        println("" + solver.getClauseNum + " clauses ...")
      }
    }

    if (res.isEmpty)
      println("No solution found!")

    res
  }

  def doMonaCheck(candidate : EdgeWeightedDigraph) {
      assert(alphSize == (1 << letterAlphabet.bitwidth))

      val cex = new java.util.ArrayList[Integer]

      MonaUtility.writeMonaDFA(prefix + ".proof.dfa", candidate,
        letterAlphabet.bitwidth, true, false)

      Thread.sleep(1000) // to avoid reading incomplete file

      if (!MonaUtility.getCexFromTeacher(prefix + ".teacher.mona",
        letterAlphabet.bitwidth, cex, true)) {
        throw new Exception("Something has gone wrong with Mona!")
      }

      if (cex.size() == 0) {
        println("=== The candidate passes the Mona test! ===\n")
        // TODO: add handler
      } else {
        val (w1, w2, len) = decodeWordPair(cex)
        val cexMsg = "(" + pp(w1, len)(system.letters) +
          ", " + pp(w2, len)(system.letters) + ")"
        println("Mona gives a counterexample: " + cexMsg)
        //TODO: add handler
      }
  }

  def decodeLetterPair(x: Int): (Int, Int) =
    (x / alphSize, x % alphSize)

  def decodeWordPair(word: JList[Integer]): (Word, Word, Int) = {
    val len = word.size
    assert(len * letterAlphabet.bitwidth <= 64)
    val (w1, w2) =
      (for (l <- word.asScala) yield decodeLetterPair(l.toInt)).unzip
    (toWord(w1: _*), toWord(w2: _*), len)
  }

}
