
package probsim.sat

import probsim._
import probsim.utils.{UnionFind, ProofProdUnionFind}
import common.finiteautomata.AutomataConverter
import common.bellmanford.EdgeWeightedDigraph
import common.encoding._
import common.elimination._

import scala.collection.JavaConverters._
import scala.collection.mutable.TreeMap
import scala.util.control.Breaks

import java.util.{List => JList, ArrayList}


class NStateBisimulation(stateNum : Int,
                         system : ExplicitStateSystem,
                         lowerBisimBound : Int=> Seq[(FixedLengthWordOps.Word,
                                                      FixedLengthWordOps.Word)],
                         bisimulations : BisimulationBank,
                         initialCheckLen : Int = 14,
                         verificationThreshold : Int = 4)
      extends AbstractNStateBisimulation(stateNum, system,
                                         lowerBisimBound, bisimulations) {
  import FixedLengthWordOps._

  //////////////////////////////////////////////////////////////////////////////

  val result : Option[EdgeWeightedDigraph] = {
    val breaks = new Breaks
    import breaks.{breakable, break}

    var largestCEXSize : Int = initialCheckLen - verificationThreshold
    var res : Option[EdgeWeightedDigraph] = None

    var n = 1
    while (res.isEmpty && solver.isSatisfiable) breakable { try {
      print("#" + n + "\t")
      n = n + 1

      val candidate =
        BoolValToAutomaton.toTransducer(solver.positiveModelVars, encoding)
//      println(candidate)

      for (len <- 0 to (largestCEXSize + verificationThreshold)) {
        val reachable = bisimulations reachableStates len
        val bisim = bisimulations bisimilarStates len

        val encodedEqv = transducer2UnionFindReachable(candidate, len)

        def backToSat = {
          largestCEXSize = largestCEXSize max len
          break
        }

        // check that we are above the lower bound
        for ((w1, w2) <- lowerBisimBound(len))
          if (encodedEqv(w1) != encodedEqv(w2)) {
            println("A: Words should be equivalent: " +
                    pp(w1, len) + ", " + pp(w2, len))
            solver addClause Array(addPair(w1, w2, len))
            backToSat
          }

        // check that the computed relation includes the identity relation
        val reachableSorted = bisimulations reachableStatesSorted len
        for (w <- reachableSorted) {
          val wl = encodeWord(w, len)
          if (!candidate.accepts(wl, wl)) {
            println("B: Reflexitivity violated:      " + pp(w, len))
            solver addClause Array(addPair(w, w, len))
            backToSat
          }
        }

        // check that the computed relation is an equivalence relation
        val eqvClasses = encodedEqv.classes
        for (w1 <- reachableSorted) {
          val wl1 = encodeWord(w1, len)
          for (w2 <- eqvClasses(encodedEqv(w1))) {
            val wl2 = encodeWord(w2, len)
            if (!candidate.accepts(wl1, wl2)) {
              println("C: Equivalence axioms violated: " +
                      pp(w1, len) + ", " + pp(w2, len))
              val path = encodedEqv.getPath(w1, w2)
              val steps =
                (for (Seq(a, b) <- path sliding 2)
                 yield -addPairSymm(a, b, len)).toList
              if (path.size > 2) {
                val bigStep = addPairSymm(path.head, path.last, len)
                solver addClause (steps ++ List(bigStep)).toArray
              }
              backToSat
            }
          }
        }

        // check that no two states are related that are not bisimilar
        for (w1 <- reachableSorted)
          for (w2 <- eqvClasses(encodedEqv(w1)))
            if (bisim(w1) != bisim(w2)) {
              println("D: Words not bisimilar:         " +
                      pp(w1, len) + ", " + pp(w2, len))
              solver addClause Array(-addPair(w1, w2, len))
              backToSat
            }

        // check that the relation is a bisimulation relation
        checkReachableBisimProp(encodedEqv, eqvClasses, len)

      }

      println("All check succeeded")
      println
      println(candidate)
      res = Some(candidate)

    } catch {
      case FoundCounterexample(len) =>
        largestCEXSize = largestCEXSize max len
    }}

    if (res.isEmpty)
      println("No solution found!")

    res
  }

}
