

package probsim.lstar

import java.io.File
import java.util
import java.util.{ArrayList => JAList, List => JList}

import common.finiteautomata.{Automata, AutomataConverter, MonaUtility}
import common.finiteautomata.lstar._
import probsim._
import probsim.utils.UnionFind

import scala.collection.JavaConverters._

class BisimulationStarWithMona(system: ExplicitStateSystem,
                               filePrefix: String,
                               exposedStateInformation: ExplicitStateBisimulation.StateExtractor =
                               ExplicitStateBisimulation.ZeroStateExtractor) {

  import FixedLengthWordOps._

  private implicit val letterAlphabet = system.letters

  private val alphSize = letterAlphabet.size
  private val bitWidth = letterAlphabet.bitwidth
  /* We need to learn automata with a bit-vector alphabet */
  /* because the counter-examples produced by Mona        */
  /* are full-ranged bit-vectors.                         */
  assert((1 << bitWidth) == alphSize)

  private val explicitBisimulations =
    BisimulationBank(system, exposedStateInformation)

  var monaElapsedTime = 0L
  var bisimElapsedTime = 0L
  var numSleeps = 0
  var numMemQueries = 0
  var numEquQueries = 0
  //////////////////////////////////////////////////////////////////////////////

  private def encodeLetterPair(x: Int, y: Int): Int =
    x * alphSize + y

  private def decodeLetterPair(x: Int): (Int, Int) =
    (x / alphSize, x % alphSize)

  private def decodeWordPair(word: JList[Integer]): (Word, Word, Int) = {
    val len = word.size
    assert(len * letterAlphabet.bitwidth <= 64)
    val (w1, w2) =
      (for (l <- word.asScala) yield decodeLetterPair(l.toInt)).unzip
    (toWord(w1: _*), toWord(w2: _*), len)
  }

  private def encodeWordPair(w1: Word, w2: Word,
                             len: Int): JList[Integer] = {
    val res = new JAList[Integer]
    for (n <- 0 until len)
      res add encodeLetterPair(ch(w1, n), ch(w2, n))
    res
  }

  //////////////////////////////////////////////////////////////////////////////

  private val lstarTeacher = new Teacher {

    def isAccepted(word: JList[Integer]): Boolean = {
      numMemQueries = numMemQueries + 1
      val (w1, w2, len) = decodeWordPair(word)
      var bisim = null: UnionFind[Word]
      var ret = false
      bisimElapsedTime = bisimElapsedTime + elapsedTime {
        bisim = explicitBisimulations.bisimilarStates(len)
        try {
          ret = bisim(w1) == bisim(w2)
        } catch {
          case _: NoSuchElementException =>
        }
      }
      ret
    }

    def isCorrectLanguage(hypothesis: Automata,
                          posCEX: JList[JList[Integer]],
                          negCEX: JList[JList[Integer]]): Boolean = {
      println("Checking hypothesis automaton, size " + hypothesis.getStates.length)
      numEquQueries = numEquQueries + 1
      val autFilename = filePrefix + ".proof.aut"
      val dfaFilename = filePrefix + ".proof.dfa"
      val monaFilename = filePrefix + ".teacher.mona"
      new File(autFilename).delete
      new File(dfaFilename).delete
      monaElapsedTime = monaElapsedTime + elapsedTime {
        MonaUtility.writeMonaDFA(autFilename, hypothesis, bitWidth, true)
        MonaUtility.writeMonaDFA(dfaFilename, hypothesis, bitWidth, true)
      }
      Thread.sleep(1000)
      numSleeps = numSleeps + 1
      val cex = new JAList[Integer]()
      monaElapsedTime = monaElapsedTime + elapsedTime {
        if (!MonaUtility.getCexFromTeacher(monaFilename, bitWidth, cex, true)) {
          throw new Exception("Error: something wrong with " + monaFilename)
        }
      }
      if (cex.size() == 0)
        return true

      val (w1, w2, len) = decodeWordPair(cex)
      val cexMsg = "(" + pp(w1, len) + ", " + pp(w2, len) + ")"
      println("CEX found: " + cexMsg)

      var bisim = null: UnionFind[Word]
      bisimElapsedTime = bisimElapsedTime + elapsedTime {
        bisim = explicitBisimulations.bisimilarStates(len)
      }
      lazy val shouldBeAccpeted: Boolean = {
        var ret = false
        bisimElapsedTime = bisimElapsedTime + elapsedTime {
          try {
            ret = bisim(w1) == bisim(w2)
          } catch {
            case _: NoSuchElementException =>
          }
        }
        ret
      }
      val isAccepted = hypothesis.accepts(cex)
      if (!isAccepted) {
        if (!shouldBeAccpeted) {
          dumpSuccessors(hypothesis, cex)
          explicitBisimulations.printClasses(len)
          throw new Exception("Error: " + cexMsg + " is expected to be a bisimular pair")
        }
        /* (w1, w2) is bisimular but not accepted by the hypothesis */
        println("  -> should be accepted: " + cexMsg)
        posCEX add cex
        return false
      } else {
        if (!shouldBeAccpeted) {
          /* (w1, w2) is non-bisimular but accepted by the hypothesis */
          println("  -> incorrectly accepted: " + cexMsg)
          negCEX add cex
          return false
        } else {
          /* (w1, w2) is both bisimular and accepted by the hypothesis */
          var foundSucc = false
          /* There should be a successor (u1, u2) that is bisimular    */
          /* but not accepted by the hypothesis.                       */
          for ((u1, _, a1) <- system.step(w1, len)) {
            for ((u2, _, a2) <- system.step(w2, len)) {
              foundSucc = true
              if (a1 == a2 && bisim(u1) == bisim(u2)) {
                val cex = encodeWordPair(u1, u2, len)
                if (!(hypothesis accepts cex)) {
                  val cexMsg2 = "(" + pp(u1, len) + ", " + pp(u2, len) + ")"
                  println("  -> a successor should be accepted: " + cexMsg2)
                  posCEX add cex
                  return false
                }
              }
            }
          }
          if (!foundSucc) {
            println("Error: cannot found any successor for " + cexMsg)
          } else {
            println("Error: " + cexMsg + " should have at least one non-bisimilar successor")
            dumpSuccessors(hypothesis, cex)
            explicitBisimulations.printClasses(len)
            val initStates = system.initialConfigurations(len)
            println("\nInital configuations of length " + len + ":")
            while (initStates.hasNext) {
              print("  " + pp(initStates.next, len))
            }
            println()
            //test(hypothesis)
          }
          throw new Exception
        }
      }
    }

    def dumpSuccessors(hypothesis: Automata, cex: JList[Integer],
                       indent: String = "",
                       visited: java.util.Set[String] = new util.HashSet[String]()): Unit = {
      val (w1, w2, len) = decodeWordPair(cex)
      val id = w1 + "#" + w2
      if (visited.contains(id)) return
      val bisim = explicitBisimulations.bisimilarStates(len)
      visited.add(id)
      if (indent == "")
        println("List of all successors:")
      for ((u2, p2, a2) <- system.step(w2, len)) {
        var print_succ = () => {
          println(indent + "|-> (N/A, " + pp(u2, len) + "), action: " + a2)
        }
        for ((u1, p1, a1) <- system.step(w1, len)) {
          if (a1 == a2) print_succ = () => {}
        }
        print_succ()
      }
      for ((u1, p1, a1) <- system.step(w1, len)) {
        var print_succ = () => {
          println(indent + "|-> (" + pp(u1, len) + ", N/A), action: " + a1)
        }
        for ((u2, p2, a2) <- system.step(w2, len)) {
          if (a1 == a2) {
            print_succ = () => {
              val cex = encodeWordPair(u1, u2, len)
              val m1 = if (bisim(u1) == bisim(u2)) "bisimilar" else "non-bisimilar"
              val m2 = if (hypothesis accepts cex) "accepted" else "not accepted"
              println(indent + "|-> (" + pp(u1, len) + ", " + pp(u2, len) + "): "
                + m1 + " and " + m2 + ", action = " + a1)
              dumpSuccessors(hypothesis, encodeWordPair(u1, u2, len), "|   " + indent, visited)
            }
          }
        }
        print_succ()
      }
      visited.remove(id)
      //println(indent + (if (hasSucc) "" else "empty"))
    }

    def test(hypothesis: Automata): Unit = {
      val t1 = new File("./test.aut")
      val t2 = new File("./test2.aut")
      t1.delete()
      t2.delete()

      MonaUtility.writeMonaDFA(t1.getPath, hypothesis, bitWidth, true)
      Thread.sleep(1000)

      val aut = MonaUtility.loadMonaDFA(t1.getPath, true)
      MonaUtility.writeMonaDFA(t2.getPath, aut, bitWidth, true)
      Thread.sleep(1000)

      import java.nio.file.Files
      val b1 = Files.readAllBytes(t1.toPath)
      val b2 = Files.readAllBytes(t2.toPath)
      assert(java.util.Arrays.equals(b1, b2))

      for (len <- 1 to 6) {
        AutomataConverter.visitWords(hypothesis, len,
          new AutomataConverter.WordVisitor {
            def apply(w: JList[Integer]): Boolean = {
              val (w1, w2, len) = decodeWordPair(w)
              if (!aut.accepts(w)) {
                throw new Exception("Error: (" + pp(w1, len) + ", " + pp(w2, len) + ")")
              }
              false
            }
          })
        AutomataConverter.visitWords(aut, len,
          new AutomataConverter.WordVisitor {
            def apply(w: JList[Integer]): Boolean = {
              if (!hypothesis.accepts(w)) {
                val (w1, w2, len) = decodeWordPair(w)
                throw new Exception("Error: (" + pp(w1, len) + ", " + pp(w2, len) + ")")
              }
              false
            }
          })
      }
    }
  } /* end of Teacher */

  def elapsedTime[R](block: => R): Long = {
    val t0 = System.currentTimeMillis()
    block // call-by-name
    val t1 = System.currentTimeMillis()
    t1 - t0
  }

  private val lstar =
    new LStar(alphSize * alphSize, lstarTeacher)
  lstar.setup
  lstar.solve
  val aut = lstar.getSolution
  println
  println("The bisimulation automaton is:")
  println(aut)
  println("#states: " + aut.getNumStates)
  println("#trans: " + aut.getNumTransitions)
  println("#sleeps: " + numSleeps)
  println("#mem. queries: " + numMemQueries)
  println("#equiv queries: " + numEquQueries)
  println
  println("Mona time: " + monaElapsedTime / 1000 + " sec.")
  println("Bisim time: " + bisimElapsedTime / 1000 + " sec.")
}
