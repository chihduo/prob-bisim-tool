

package probsim.lstar

import common.finiteautomata.lstar._
import common.finiteautomata.Automata
import common.finiteautomata.AutomataConverter

import probsim._

import scala.collection.JavaConverters._

import java.util.{List => JList, ArrayList}

class BisimulationStar(system: ExplicitStateSystem,
                       exposedStateInformation: ExplicitStateBisimulation.StateExtractor =
                       ExplicitStateBisimulation.ZeroStateExtractor,
                       bisimCache: Option[BisimulationBank] = None,
                       maxCheckLen: Int = 3,
                       minCheckLen: Int = 0) {

  import FixedLengthWordOps._

  private implicit val letterAlphabet = system.letters

  private val explicitBisimulations: BisimulationBank =
    bisimCache.getOrElse(BisimulationBank(system,
                                          exposedStateInformation))

  //////////////////////////////////////////////////////////////////////////////

  val alphSize = letterAlphabet.size

  private val lstarTeacher = new Teacher {

    var currentCheckLen = 0

    def isAccepted(word: JList[Integer]): Boolean = {
      val (w1, w2, len) = decodeWordPair(word)
      //      println(pp(w1, len) + " bisim " + pp(w2, len) + " ?")
      //      print(".")
      val reachable = explicitBisimulations.reachableStates(len)
      val bisim = explicitBisimulations.bisimilarStates(len)
      reachable(w1) && reachable(w2) && bisim(w1) == bisim(w2)
    }

    def isCorrectLanguage(hypothesis: Automata,
                          posCEX: JList[JList[Integer]],
                          negCEX: JList[JList[Integer]]): Boolean = {
      //      println
      println("Checking hypothesis automaton, size " + hypothesis.getStates.length)
      //      println(hypothesis)

      for (len <- minCheckLen to maxCheckLen) {
        val reachable = explicitBisimulations.reachableStates(len)
        val bisim = explicitBisimulations.bisimilarStates(len)

        // check that the mappings of the finite bisimulation are accepted
        // TODO: it would probably be more efficient to do a full reflexivity,
        // symmetry, transitivity check
        val eqvClasses = bisim.classes
        for (repr <- reachable)
          if (bisim(repr) == repr) {
            val reprClass = eqvClasses(repr)
            for (w1 <- reprClass)
              for (w2 <- reprClass) {
                val pair = encodeWordPair(w1, w2, len)
                if (!(hypothesis accepts pair)) {
                  println("should be accepted: (" +
                    pp(w1, len) + ", " + pp(w2, len) + ")")
                  posCEX add pair
                  return false
                }
              }
          }

        // check that no extra words are accepted
        var foundCEX = false
        AutomataConverter.visitWords(hypothesis, len,
          new AutomataConverter.WordVisitor {
            def apply(w: JList[Integer]): Boolean = {
              val (w1, w2, _) = decodeWordPair(w)
              if (!(reachable contains w1) ||
                !(reachable contains w2) ||
                bisim(w1) != bisim(w2)) {
                println("incorrectly accepted: (" +
                  pp(w1, len) + ", " + pp(w2, len) + ")")
                negCEX add w
                foundCEX = true
              }
              foundCEX
            }
          })

        if (foundCEX)
          return false

      }

      true
    }

  }

  private val lstar = new LStar(alphSize * alphSize, lstarTeacher)
  lstar.setup
  lstar.solve

  val result: Automata = lstar.getSolution

  println
  println("There seems to be a bisimulation automaton with "
    + result.getStates.length + " states.")
}
