

package probsim.lstar

import java.io.File
import java.util
import java.util.{ArrayList => JAList, List => JList}

import common.finiteautomata.lstar._
import common.finiteautomata.{Automata, AutomataConverter, MonaUtility}
import probsim._

import scala.collection.JavaConverters._

class Test(letters: Enumeration,
           bitWidth: Int) {

  val exposedStateInformation = ExplicitStateBisimulation.ZeroStateExtractor

  import FixedLengthWordOps._

  private implicit val letterAlphabet = LetterAlphabet(letters)

  private val alphSize = 1 << bitWidth

  /* We need to learn automata with a bit-vector alphabet */
  /* because the counter-examples produced by Mona        */
  /* are full-ranged bit-vectors.                         */
  assert((1 << bitWidth) == alphSize)

  //////////////////////////////////////////////////////////////////////////////

  private def encodeLetterPair(x: Int, y: Int): Int =
    x * alphSize + y

  private def decodeLetterPair(x: Int): (Int, Int) =
    (x / alphSize, x % alphSize)

  private def decodeWordPair(word: JList[Integer]): (Word, Word, Int) = {
    val len = word.size
    val (w1, w2) =
      (for (l <- word.asScala) yield decodeLetterPair(l.toInt)).unzip
    (toWord(w1: _*), toWord(w2: _*), len)
  }

  private def encodeWordPair(w1: Word, w2: Word,
                             len: Int): JList[Integer] = {
    val res = new JAList[Integer]
    for (n <- 0 until len)
      res add encodeLetterPair(ch(w1, n), ch(w2, n))
    res
  }

  def check(hypothesis: Automata) {
    val filePrefix = "test"
    println("Checking hypothesis automaton, size " + hypothesis.getStates.length)
    val autFilename = filePrefix + ".aut"
    val dfaFilename = filePrefix + ".dfa"
    val monaFilename = filePrefix + ".mona"
    new File(autFilename).delete
    new File(dfaFilename).delete
    MonaUtility.writeMonaDFA(autFilename, hypothesis, bitWidth, true)
    MonaUtility.writeMonaDFA(dfaFilename, hypothesis, bitWidth, true)
    Thread.sleep(1000)
    val cex = new JAList[Integer]()
    if (!MonaUtility.getCexFromTeacher(monaFilename, bitWidth, cex, true)) {
      throw new Exception("Error: something wrong with " + monaFilename)
    }
    if (cex.size() > 0) {
      val (w1, w2, len) = decodeWordPair(cex)
      val cexMsg = "(" + pp(w1, len) + ", " + pp(w2, len) + ")"
      println("CEX found: " + cexMsg)
    } else {
      println("Mona test is passed!")
    }
  }


  def test(hypothesis: Automata): Unit = {
    val t1 = new File("./test.aut")
    val t2 = new File("./test2.aut")
    t1.delete()
    t2.delete()

    MonaUtility.writeMonaDFA(t1.getPath, hypothesis, bitWidth, true)
    Thread.sleep(1000)

    val aut = MonaUtility.loadMonaDFA(t1.getPath, true)
    MonaUtility.writeMonaDFA(t2.getPath, aut, bitWidth, true)
    Thread.sleep(1000)

    import java.nio.file.Files
    val b1 = Files.readAllBytes(t1.toPath)
    val b2 = Files.readAllBytes(t2.toPath)
    assert(java.util.Arrays.equals(b1, b2))

    for (len <- 1 to 6) {
      AutomataConverter.visitWords(hypothesis, len,
        new AutomataConverter.WordVisitor {
          def apply(w: JList[Integer]): Boolean = {
            val (w1, w2, len) = decodeWordPair(w)
            if (!aut.accepts(w)) {
              throw new Exception("Error: (" + pp(w1, len) + ", " + pp(w2, len) + ")")
            }
            false
          }
        })
      AutomataConverter.visitWords(aut, len,
        new AutomataConverter.WordVisitor {
          def apply(w: JList[Integer]): Boolean = {
            if (!hypothesis.accepts(w)) {
              val (w1, w2, len) = decodeWordPair(w)
              throw new Exception("Error: (" + pp(w1, len) + ", " + pp(w2, len) + ")")
            }
            false
          }
        })
    }
  }
}
