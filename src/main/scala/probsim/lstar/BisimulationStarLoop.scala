

package probsim.lstar

import java.util.{ArrayList, List => JList}

import common.finiteautomata.{Automata, AutomataConverter, MonaUtility}
import common.finiteautomata.lstar._
import probsim.ExplicitStateBisimulation.StateExtractor

import probsim._

import scala.collection.JavaConverters._

class BisimulationStarLoop(system: ExplicitStateSystem,
                           prefix: String,
                           exposedStateInformation: StateExtractor,
                           maxCheckLen: Int = 5) {

  val USE_CEX_FROM_MONA = false

  import FixedLengthWordOps._

  private implicit val letterAlphabet = system.letters

  private val explicitBisimulations: BisimulationBank =
    BisimulationBank(system, exposedStateInformation)

  //////////////////////////////////////////////////////////////////////////////

  private val alphSize = letterAlphabet.size
  private var lenUpperBnd = maxCheckLen

  def encodeLetterPair(x: Int, y: Int): Int =
    x * alphSize + y

  def decodeLetterPair(x: Int): (Int, Int) =
    (x / alphSize, x % alphSize)

  def decodeWordPair(word: JList[Integer]): (Word, Word, Int) = {
    val len = word.size
    assert(len * letterAlphabet.bitwidth <= 64)
    val (w1, w2) =
      (for (l <- word.asScala) yield decodeLetterPair(l.toInt)).unzip
    (toWord(w1: _*), toWord(w2: _*), len)
  }

  def encodeWordPair(w1: Word, w2: Word,
                     len: Int): JList[Integer] = {
    val res = new ArrayList[Integer]
    for (n <- 0 until len)
      res add encodeLetterPair(ch(w1, n), ch(w2, n))
    res
  }

  //////////////////////////////////////////////////////////////////////////////

  private val lstarTeacher = new Teacher {

    def isAccepted(word: JList[Integer]): Boolean = {
      val (w1, w2, len) = decodeWordPair(word)
      //      println(pp(w1, len) + " bisim " + pp(w2, len) + " ?")
      //      print(".")
      val reachable = explicitBisimulations.reachableStates(len)
      val bisim = explicitBisimulations.bisimilarStates(len)
      reachable(w1) && reachable(w2) && bisim(w1) == bisim(w2)
    }

    def isCorrectLanguage(hypothesis: Automata,
                          posCEX: JList[JList[Integer]],
                          negCEX: JList[JList[Integer]]): Boolean = {

      println("Checking hypothesis automaton, size " + hypothesis.getStates.length)
      var lenLowerBnd = 0
      while (true) {
        for (len <- lenLowerBnd to lenUpperBnd) {
          val reachable = explicitBisimulations.reachableStates(len)
          val bisim = explicitBisimulations.bisimilarStates(len)

          // check that the mappings of the finite bisimulation are accepted
          // TODO: it would probably be more efficient to do a full reflexivity,
          // symmetry, transitivity check
          val eqvClasses = bisim.classes
          for (repr <- reachable)
            if (bisim(repr) == repr) {
              val reprClass = eqvClasses(repr)
              for (w1 <- reprClass)
                for (w2 <- reprClass) {
                  val pair = encodeWordPair(w1, w2, len)
                  if (!(hypothesis accepts pair)) {
                    println("should be accepted: (" +
                      pp(w1, len) + ", " + pp(w2, len) + ")")
                    posCEX add pair
                    return false
                  }
                }
            }

          // check that no extra words are accepted
          var foundCEX = false
          AutomataConverter.visitWords(hypothesis, len,
            new AutomataConverter.WordVisitor {
              def apply(w: JList[Integer]): Boolean = {
                val (w1, w2, _) = decodeWordPair(w)
                val cex = pp(w1, len) + ", " + pp(w2, len)
                if (!(reachable contains w1) ||
                  !(reachable contains w2) ||
                  bisim(w1) != bisim(w2)) {
                  var isCex = false
                  //                var isBisim = false
                  //                try {
                  //                  isBisim = bisim(w1) == bisim(w2)
                  //                  isCex = (reachable contains w1) & (reachable contains w2) & !isBisim
                  //                } catch {
                  //                  case _ => isCex = true
                  //                }
                  if (isCex) {
                    println("incorrectly accepted: (" + cex + ")")
                    //                  println("is bisimilar: " + isBisim)
                    negCEX add w
                    foundCEX = true
                  }
                }
                foundCEX
              }
            })
          if (foundCEX)
            return false
        }

        println("A candidate of " + hypothesis.getStates.length
          + " states is found. Verifying it with Mona...")

        MonaUtility.writeMonaDFA(prefix + ".proof.dfa", hypothesis,
          letterAlphabet.bitwidth, true)
        Thread.sleep(1000)

        val cex = new java.util.ArrayList[Integer]()
        if (!MonaUtility.getCexFromTeacher(prefix + ".teacher.mona",
          letterAlphabet.bitwidth, cex, true)) {
          throw new Exception("Something wrong with Mona")
        }

        if (cex.size() == 0) {
          println("=== The hypothesis passes the Mona test! ===\n")
          println("The automaton is saved to: " + prefix + ".proof.dfa")
          return true
        } else {
          val (w1, w2, len) = decodeWordPair(cex)
          val cexMsg = "(" + pp(w1, len)(system.letters) +
            ", " + pp(w2, len)(system.letters) + ")"

          println("Mona gives a counterexample: " + cexMsg)

          if (USE_CEX_FROM_MONA) {
            analyzeCEX(hypothesis, cex, posCEX, negCEX)
            return false
          }
          lenLowerBnd = lenUpperBnd + 1
          lenUpperBnd = len
          //assert(lenUpperBnd >= lenLowerBnd)
          lenUpperBnd = Math.max(lenLowerBnd + 1, lenUpperBnd)
          println("==== MaxCheckLen is set to " + lenUpperBnd + " ====")
        }
      }
      return false
    }

    def analyzeCEX(hypothesis: Automata,
                   cex: JList[Integer],
                   posCEX: JList[JList[Integer]],
                   negCEX: JList[JList[Integer]]): Unit = {

      val (w1, w2, len) = decodeWordPair(cex)
      val cexMsg = "(" + pp(w1, len)(system.letters) + ", " +
        pp(w2, len)(system.letters) + ")"
      lazy val bisim = explicitBisimulations.bisimilarStates(len)
      lazy val shouldBeAccpeted = {
        val reachable = explicitBisimulations.reachableStates(len)
        (reachable contains w1) & (reachable contains w2) & bisim(w1) == bisim(w2)
      }
      val isAccepted = hypothesis.accepts(cex)
      if (!isAccepted) {
        if (!shouldBeAccpeted) {
          explicitBisimulations.printClasses(len)
          println("Error: " + cexMsg + " is expected to be a bisimular pair")
          dumpSuccessors(hypothesis, cex)
          System.exit(-1)
        }
        /* (w1, w2) is bisimular but not accepted by the hypothesis */
        println("  -> should be accepted: " + cexMsg)
        posCEX add cex
      } else {
        if (!shouldBeAccpeted) {
          /* (w1, w2) is non-bisimular but accepted by the hypothesis */
          println("  -> incorrectly accepted: " + cexMsg)
          negCEX add cex
        } else {
          /* (w1, w2) is both bisimular and accepted by the hypothesis */
          var foundSucc = false
          /* There should be a successor (u1, u2) that is bisimular    */
          /* but not accepted by the hypothesis.                       */
          for ((u1, _, a1) <- system.step(w1, len)) {
            for ((u2, _, a2) <- system.step(w2, len)) {
              foundSucc = true
              if (a1 == a2 && bisim(u1) == bisim(u2)) {
                val cex = encodeWordPair(u1, u2, len)
                if (!(hypothesis accepts cex)) {
                  val cexMsg2 = "(" + pp(u1, len) + ", " + pp(u2, len) + ")"
                  println("  -> a successor should be accepted: " + cexMsg2)
                  posCEX add cex
                  return
                }
              }
            }
          }
          if (!foundSucc) {
            println("Error: cannot found any successor for " + cexMsg)
          } else {
            explicitBisimulations.printClasses(len)
            println("Error: " + cexMsg + " should have at least one non-bisimilar successor")
            dumpSuccessors(hypothesis, cex)
            val initStates = system.initialConfigurations(len)
            println("\nInital configuations of length " + len + ":")
            while (initStates.hasNext) {
              print("  " + pp(initStates.next, len))
            }
            println()
            //test(hypothesis)
          }
          System.exit(-1)
        }
      }
    }

    def dumpSuccessors(hypothesis: Automata, cex: JList[Integer]): Unit = {
      val (w1, w2, len) = decodeWordPair(cex)
      val bisim = explicitBisimulations.bisimilarStates(len)
      var hasSucc = false
      print("List of all successors: ")
      for ((u1, p1, a1) <- system.step(w1, len)) {
        for ((u2, p2, a2) <- system.step(w2, len)) {
          hasSucc = true
          val cex = encodeWordPair(u1, u2, len)
          val m1 = if (bisim(u1) == bisim(u2)) "bisimilar" else "non-bisimilar"
          val m2 = if (hypothesis accepts cex) "accepted" else "not accepted"
          print("\n  (" + pp(u1, len) + ", " + pp(u2, len) + "): " + m1 + " and " + m2)
        }
      }
      println(if (hasSucc) "" else "empty")
    }

  } // end of LStar teacher

  private val lstar = new LStar(alphSize * alphSize, lstarTeacher)
  lstar.setup
  lstar.solve
}
