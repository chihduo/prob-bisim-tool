
package probsim.lstar

import common.finiteautomata.lstar._
import common.finiteautomata.Automata
import common.finiteautomata.AutomataConverter

import probsim._

import scala.collection.JavaConverters._

import java.util.{List => JList, ArrayList}

class ReachabilityStar(system: ExplicitStateSystem,
                       reachabilityBank : ReachabilityBank,
                       initialCheckLen : Int = 12,
                       verificationThreshold : Int = 3) {

  import FixedLengthWordOps._

  private implicit val letterAlphabet = system.letters
  private val alphSize = letterAlphabet.size

  private var largestCEXSize : Int = initialCheckLen - verificationThreshold

  private val lstarTeacher = new Teacher {

    def isAccepted(word: JList[Integer]): Boolean = {
      val (w, len) = jlist2Word(word)
      reachabilityBank(len) contains w
    }

    def isCorrectLanguage(hypothesis: Automata,
                          posCEX: JList[JList[Integer]],
                          negCEX: JList[JList[Integer]]): Boolean = {
      println("Checking hypothesis automaton, size " + hypothesis.getStates.length)

      for (len <- 0 until (largestCEXSize + verificationThreshold)) {
        val reachableStates = reachabilityBank(len)

        // check that reachable states are accepted by the automaton
        for (w <- reachableStates)
          if (!(hypothesis accepts word2JList(w, len))) {
            println("should be accepted: " + pp(w, len))
            posCEX add word2JList(w, len)
            largestCEXSize = largestCEXSize max len
            return false
          }

        // check that no extra words are accepted
        var foundCEX = false
        AutomataConverter.visitWords(hypothesis, len,
          new AutomataConverter.WordVisitor {
            def apply(wl: JList[Integer]): Boolean = {
              val (w, _) = jlist2Word(wl)
              if (!reachableStates(w)) {
                println("incorrectly accepted: " + pp(w, len))
                negCEX add wl
                largestCEXSize = largestCEXSize max len
                foundCEX = true
              }
              foundCEX
            }
          })

        if (foundCEX)
          return false
      }

      true
    }

  }
  
  private val lstar = new LStar(alphSize, lstarTeacher)
  lstar.setup
  lstar.solve

  val result: Automata = lstar.getSolution

  println
  println("There seems to be a reachability automaton with "
    + result.getStates.length + " states:")
  println(result)

}
