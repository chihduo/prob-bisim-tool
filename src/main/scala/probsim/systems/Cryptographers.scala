

package probsim.systems

import probsim._

object Cryptographers extends ExplicitStateSystem {
  import FixedLengthWordOps._

  /* size of the alphabet needs to be a power of two */
  object Letters extends Enumeration {
    val _, N, H, P, T, A, U, s0 = Value
  }

  object Actions extends Enumeration {
    val Toss, Announced0, Announced1 = Value
  }

  implicit val letters = LetterAlphabet(Letters)
  val actions = ActionAlphabet(Actions)

  import Letters._
  import Actions._
  import letters.{letter2Pattern, wrapLetter}
  import actions.wrapAction

  val probDenom = 2

  def initialConfigurations(len : Int) : Iterator[Word] =
    if (len < 6 || len % 2 != 0) {
      Iterator.empty
    } else {
      val cryptoNum = len / 2

      (Iterator single sequence(repeatLetters(cryptoNum)(N, U) : _*)) ++
      (for (pos <- (0 until cryptoNum).iterator) yield
         sequence((repeatLetters(pos)                (N, U) ++
                   repeatLetters(1)                  (P, U) ++
                   repeatLetters(cryptoNum - pos - 1)(N, U)) : _*))
    }

  import Rewriting._

  private val stepRules = List(
    (BEG ^^ N ^^ U)        ==> (N, T)    probability 1 action Toss,
    (BEG ^^ N ^^ U)        ==> (N, H)    probability 1 action Toss,
    (BEG ^^ P ^^ U)        ==> (P, T)    probability 1 action Toss,
    (BEG ^^ P ^^ U)        ==> (P, H)    probability 1 action Toss,

    (T ^^ N ^^ U)          ==> (T, N, T) probability 1 action Toss,
    (T ^^ N ^^ U)          ==> (T, N, H) probability 1 action Toss,
    (T ^^ P ^^ U)          ==> (T, P, T) probability 1 action Toss,
    (T ^^ P ^^ U)          ==> (T, P, H) probability 1 action Toss,
    (H ^^ N ^^ U)          ==> (H, N, T) probability 1 action Toss,
    (H ^^ N ^^ U)          ==> (H, N, H) probability 1 action Toss,
    (H ^^ P ^^ U)          ==> (H, P, T) probability 1 action Toss,
    (H ^^ P ^^ U)          ==> (H, P, H) probability 1 action Toss,

    (T ^^ BEG ^^ N ^^ T)   =~=> (T, A, T) probability 2 action Announced0,
    (T ^^ BEG ^^ N ^^ H)   =~=> (T, A, H) probability 2 action Announced1,
    (H ^^ BEG ^^ N ^^ T)   =~=> (H, A, T) probability 2 action Announced1,
    (H ^^ BEG ^^ N ^^ H)   =~=> (H, A, H) probability 2 action Announced0,
    (T ^^ BEG ^^ P ^^ T)   =~=> (T, A, T) probability 2 action Announced1,
    (T ^^ BEG ^^ P ^^ H)   =~=> (T, A, H) probability 2 action Announced0,
    (H ^^ BEG ^^ P ^^ T)   =~=> (H, A, T) probability 2 action Announced0,
    (H ^^ BEG ^^ P ^^ H)   =~=> (H, A, H) probability 2 action Announced1,

    (A ^^ T ^^ N ^^ T)     ==> (A, T, A, T) probability 2 action Announced0,
    (A ^^ T ^^ N ^^ H)     ==> (A, T, A, H) probability 2 action Announced1,
    (A ^^ H ^^ N ^^ T)     ==> (A, H, A, T) probability 2 action Announced1,
    (A ^^ H ^^ N ^^ H)     ==> (A, H, A, H) probability 2 action Announced0,
    (A ^^ T ^^ P ^^ T)     ==> (A, T, A, T) probability 2 action Announced1,
    (A ^^ T ^^ P ^^ H)     ==> (A, T, A, H) probability 2 action Announced0,
    (A ^^ H ^^ P ^^ T)     ==> (A, H, A, T) probability 2 action Announced0,
    (A ^^ H ^^ P ^^ H)     ==> (A, H, A, H) probability 2 action Announced1
  )

  def step(config : Word, len : Int) : Iterator[(Word, Int, Int)] = try {
    rewrite(config, len, probDenom, actions, stepRules)
  } catch {
    case t : Throwable =>
      Iterator.empty
  }
}
