

package probsim.systems

import probsim._

object IntCounterActions extends ExplicitStateSystem {
  import FixedLengthWordOps._

  object Letters extends Enumeration {
    val d0, d1 = Value
  }

  object Actions extends Enumeration {
    val Inc, Dec = Value
  }

  implicit val letters = LetterAlphabet(Letters)
  val actions = ActionAlphabet(Actions)

  import Letters._
  import Actions._
  import letters.{letter2Pattern, wrapLetter}
  import actions.wrapAction

  val probDenom = 4

  def initialConfigurations(len : Int) : Iterator[Word] =
    if (len < 1)
      Iterator.empty
    else if (len % 2 == 0)
      Iterator single multSequence((d0, len / 2 - 1), (d1, 2), (d0, len / 2 - 1))
    else
      Iterator single multSequence((d0, len / 2), (d1, 1), (d0, len / 2))

  import Rewriting._

  private val stepRules = List(
    (d1 ^^ d0)        ==> (d1, d1) probability 3 action Inc,
    (d1 ^^ d0)        ==> (d0, d0) probability 1 action Dec,
    (d0 ^^ d1)        ==> (d1, d1) probability 1 action Inc,
    (d0 ^^ d1)        ==> (d0, d0) probability 3 action Dec,
    (d1 ^^ END)       ==> (d1)     probability 3 action Inc,
    (d1 ^^ END)       ==> (d0)     probability 1 action Dec,
    (BEG ^^ d1)       ==> (d0)     probability 3 action Dec,
    (BEG ^^ d1)       ==> (d1)     probability 1 action Inc
  )

  def step(config : Word, len : Int) : Iterator[(Word, Int, Int)] =
    rewrite(config, len, probDenom, actions, stepRules)
}
