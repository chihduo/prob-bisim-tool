package probsim.systems

import common.finiteautomata.{Automata, MonaUtility}

import probsim._

object DiningCryptoSym {

  object Letters extends Enumeration {
    // TODO: load letters from input
    val s0, s1, s2, s3, s4, s5, s6, s7 = Value
  }

  implicit val letters = LetterAlphabet(Letters)
  val actions = UnitActionAlphabet
  val probDenom = 2


}
