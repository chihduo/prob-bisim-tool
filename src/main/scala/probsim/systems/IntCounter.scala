

package probsim.systems

import probsim._

object IntCounter extends ExplicitStateSystem {
  import FixedLengthWordOps._

  object Letters extends Enumeration {
    val d0, d1 = Value
  }

  implicit val letters = LetterAlphabet(Letters)
  val actions = UnitActionAlphabet

  import Letters._
  import letters.{letter2Pattern, wrapLetter}

  val probDenom = 4

  def initialConfigurations(len : Int) : Iterator[Word] =
    for (n <- (0 to len).iterator) yield
      multSequence((d1, n), (d0, len - n))

  import Rewriting._

  private val stepRules = List(
    (d1 ^^ d0)        ==> (d1, d1) probability 1,
    (d1 ^^ d0)        ==> (d0, d0) probability 3,
    (d1 ^^ d1 ^^ END) ==> (d1, d1) probability 1,
    (d1 ^^ d1 ^^ END) ==> (d1, d0) probability 3,
    (BEG ^^ d0 ^^ d0) ==> (d1, d0) probability 1,
    (BEG ^^ d0 ^^ d0) ==> (d0, d0) probability 3
  )

  def step(config : Word, len : Int) : Iterator[(Word, Int, Int)] =
    rewrite(config, len, probDenom, actions, stepRules)
}
