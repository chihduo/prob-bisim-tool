package probsim.systems

import common.finiteautomata.{Automata, AutomataConverter, State}

import scala.collection.JavaConverters._
import scala.collection.mutable.ArrayBuffer
import java.util.{ArrayList => JAList, List => JList}

import probsim.FixedLengthWordOps.Word
import probsim._

class RegularPTS(initConfigs: Automata,
                 transRels: Array[Automata],
                 actionLetters: Enumeration,
                 probs: Array[Int],
                 probDenominator: Int,
                 configAlphabet: Enumeration)
  extends ExplicitStateSystem {
  /*
  var numStates = 0
  var numTrans = 0
  for (aut <- transRels) {
    numStates = numStates + aut.getNumStates
    numTrans = numTrans + aut.getNumTransitions
  }
  System.out.println("#states of Init: " + initConfigs.getNumStates)
  System.out.println("#trans of Init: " + initConfigs.getNumTransitions)
  System.out.println("#states of Trans: " + numStates)
  System.out.println("#trans of Trans: " + numTrans)
  System.exit(1)
  */

  import FixedLengthWordOps._

  implicit val letters = LetterAlphabet(configAlphabet)
  val actions = ActionAlphabet(actionLetters)
  val probDenom = probDenominator
  val coder: Encoding = new Encoding(letters.size)

  def initialConfigurations(len: Int): Iterator[Word] = {
    val list: JList[JList[Integer]] = AutomataConverter.getWords(initConfigs, len)
    for (word <- list.asScala.iterator)
      yield coder.decodeWord(word)
  }

  def step(config: Word, len: Int): Iterator[(Word, Int, Int)] = {
    assert(len <= 64)
    val result = new ArrayBuffer[(Word, Int, Int)]
    var actionID = 0
    for ((transRel, al) <- transRels.iterator zip actionsAcceptedLengths.iterator) {
      val init = transRel.getStates()(transRel.getInitState)
      exploreSuccessors(transRel, init, config, 0, len, al, zeroWord, probs(actionID), actionID, result)
      actionID = actionID + 1
    }
    result.iterator
  }

  /**
    * Bit-sets with the lengths of words accepted at each automaton state
    */
  private val actionsAcceptedLengths: IndexedSeq[Array[Long]] =
    for (aut <- transRels) yield {
      val states = aut.getStates
      val acceptedLengths = new Array[Long](states.size)

      for (i <- 0 until states.size)
        if (aut.getAcceptingStates contains i)
          acceptedLengths(i) = 1l

      var cont = true
      while (cont) {
        cont = false
        for (i <- 0 until states.size) {
          var reachableAccept: Long = 0l
          for (id <- states(i).getDest.asScala)
            reachableAccept = reachableAccept | acceptedLengths(id)
          val oldAccept = acceptedLengths(i)
          val newAccept = (reachableAccept << 1) | oldAccept
          if (newAccept != oldAccept) {
            cont = true
            acceptedLengths(i) = newAccept
          }
        }
      }

      acceptedLengths
    }

  def exploreSuccessors(transRel: Automata,
                        state: State, // current state of the transducer
                        config: Word, // predecessor
                        pos: Int,
                        wordLength: Int, // length of the words to explore
                        acceptedLengths: Array[Long],
                        currentWord: Word,
                        prob: Int,
                        action: Int,
                        result: ArrayBuffer[(Word, Int, Int)]): Unit = {
    if ((acceptedLengths(state.getId) & (1l << (wordLength - pos))) == 0l)
      return

    if (pos >= wordLength) {
      result += ((currentWord, prob, action))
    } else {
      val symbol = coder.charAt(config, pos)
      val labelIt = state.getOutgoingLabels.iterator
      while (labelIt.hasNext) {
        val label = labelIt.next
        val (left, right) = coder.decodeLetterPair(label)
        if (symbol == left) {
          val newWord = update(currentWord, pos, right)
          val sidIt = (state getDest label).iterator
          while (sidIt.hasNext) {
            val sid = sidIt.next
            val next = transRel.getStates()(sid)
            exploreSuccessors(transRel, next,
              config, pos + 1, wordLength, acceptedLengths, newWord,
              prob, action, result)
          }
        }
      }
    }
  }
}

class Encoding(alphabetSize: Int) {

  val alphaBits: Int = numBits(alphabetSize)
  val alphaMask: Int = (1 << alphaBits) - 1

  private def numBits(number: Int): Int = {
    var ret = 1
    while ((1 << ret) < number)
      ret = ret + 1
    ret
  }

  def charAt(w: Word, pos: Int): Int = {
    (w >>> (pos * alphaBits)).toInt & alphaMask
  }

  /**
    * Encode (w1, w2) to (MSB) w1 :: w2 (LSB)
    */
  def encodeLetterPair(w1: Int, w2: Int): Int = {
    assert(numBits(w1) <= alphaBits)
    assert(numBits(w2) <= alphaBits)
    ((w1 & alphaMask) << alphaBits) | (w2 & alphaMask)
  }

  /**
    * Decode (MSB) w1 :: w2 (LSB) to (w1, w2)
    */
  def decodeLetterPair(x: Int): (Int, Int) = {
    //    assert(numBits(x) <= 2 * alphaBits)
    ((x >>> alphaBits) & alphaMask, x & alphaMask)
  }

  /**
    * Decode { u_i :: v_i : 0 <= i < n } to (u, v, n)
    */
  def decodeWordPair(word: JList[Integer]): (Word, Word, Int) = {
    val len = word.size()
    assert(len * alphaBits <= 64)
    var w1 = 0L
    var w2 = 0L
    for (bits <- word.asScala.reverse) {
      val (v1, v2) = decodeLetterPair(bits)
      w1 = (w1 << alphaBits) | v1
      w2 = (w2 << alphaBits) | v2
    }
    (w1, w2, len)
  }

  def decodeWord(word: JList[Integer]): Word = {
    val (_, ret, _) = decodeWordPair(word)
    ret
  }

  def encodeWordPair(w1: Word, w2: Word,
                     len: Int): JList[Integer] = {
    val ret = new JAList[Integer]
    for (n <- 0 until len)
      ret add encodeLetterPair(charAt(w1, n), charAt(w2, n))
    ret
  }
}
