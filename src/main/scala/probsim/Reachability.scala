


package probsim

import scala.collection.{Set => GSet}
import scala.collection.mutable.{HashSet => MHashSet, HashMap => MHashMap,
                                 ArrayStack}

object ExplicitStateReachable {

  import FixedLengthWordOps._

  def apply(system : FixedLengthExplicitStateSystem) : GSet[Word] = {
    val res = new MHashSet[Word]
    val todo = new ArrayStack[Word]

    for (w <- system.initialConfigurations)
      if (res add w)
        todo push w

    while (!todo.isEmpty) {
      val next = todo.pop
      for ((w, p, _) <- system step next) {
        assert(p > 0)
        if (res add w)
          todo push w
      }
    }

    res
  }

}

////////////////////////////////////////////////////////////////////////////////

class ReachabilityBank(system: ExplicitStateSystem) {

  import FixedLengthWordOps._

  private val reachableSets = new MHashMap[Int, GSet[Word]]
  private val reachableSetsSorted = new MHashMap[Int, Seq[Word]]

  def apply(len: Int): GSet[Word] =
    reachableSets.getOrElseUpdate(len,
      ExplicitStateReachable(ExplicitStateSystemInstance(system, len)))

  def sorted(len : Int) : Seq[Word] =
    reachableSetsSorted.getOrElseUpdate(len, apply(len).toVector.sorted)

}
