
package probsim

object Rewriting {

  private val verifyProbs = true

  import FixedLengthWordOps._

  abstract class Pattern {
    def ^^(that : Pattern) = CatPattern(this, that)

//    def ==>(replacement : Int*) =
//      SimpleRule(this, replacement.toSeq)

    def ==>(replacement : LetterAlphabet.WrappedLetter*) =
      SimpleRule(this, replacement.toSeq map (_.value))

    def =~=>(replacement : LetterAlphabet.WrappedLetter*) =
      CircRule(this, replacement.toSeq map (_.value))

    def matches(w : Word, len : Int, pos : Int)
               (implicit alph : Alphabet) : Option[Int]

    def matchesCirc(w : Word, len : Int, pos : Int)
                   (implicit alph : Alphabet) : Option[Int]
  }

  case object BEG extends Pattern {

    def matches(w : Word, len : Int, pos : Int)
               (implicit alph : Alphabet) : Option[Int] =
      if (pos == 0) Some(0) else None

    def matchesCirc(w : Word, len : Int, pos : Int)
                   (implicit alph : Alphabet) : Option[Int] =
      matches(w, len, pos)
  }

  case object END extends Pattern {

    def matches(w : Word, len : Int, pos : Int)
               (implicit alph : Alphabet) : Option[Int] =
      if (pos == len) Some(len) else None

    def matchesCirc(w : Word, len : Int, pos : Int)
                   (implicit alph : Alphabet) : Option[Int] =
      matches(w, len, pos)
  }

  case class CharPattern(letter : Int) extends Pattern {

    def matches(w : Word, len : Int, pos : Int)
               (implicit alph : Alphabet) : Option[Int] =
      if (pos < len && ch(w, pos) == letter)
        Some(pos + 1)
      else
        None

    def matchesCirc(w : Word, len : Int, pos : Int)
                   (implicit alph : Alphabet) : Option[Int] =
      for (newPos <- matches(w, len, pos)) yield (newPos % len)
  }

  case class CatPattern(left : Pattern, right : Pattern) extends Pattern {

    def matches(w : Word, len : Int, pos : Int)
               (implicit alph : Alphabet) : Option[Int] =
      for (newPos1 <- left.matches(w, len, pos);
           newPos2 <- right.matches(w, len, newPos1)) yield newPos2

    def matchesCirc(w : Word, len : Int, pos : Int)
                   (implicit alph : Alphabet) : Option[Int] =
      for (newPos1 <- left.matchesCirc(w, len, pos);
           newPos2 <- right.matchesCirc(w, len, newPos1)) yield newPos2
  }

  //////////////////////////////////////////////////////////////////////////////

  abstract class Rule(val prob : Int, val action : Int) {

    def probability(prob : Int) : Rule

    def action(act : ActionAlphabet.WrappedAction) : Rule = action(act.value)

    def action(act : Int) : Rule

    def rewrite(w : Word, len : Int)
               (implicit alph : Alphabet) : Iterator[(Word, Int, Int)]
  }

  case class SimpleRule(pattern : Pattern, replacement : Seq[Int],
                        _prob : Int = 0, _action : Int = 0)
             extends Rule(_prob, _action) {

    def probability(p : Int) : Rule = copy(_prob = p)
    def action(act : Int) : Rule = copy(_action = act)

    def rewrite(w : Word, len : Int)
               (implicit alph : Alphabet) : Iterator[(Word, Int, Int)] = {
      for (i <- (0 until (len - replacement.size + 1)).iterator;
           if pattern.matches(w, len, i).isDefined)
      yield (update(w, i, replacement), prob, action)
    }
  }

  case class CircRule(pattern : Pattern, replacement : Seq[Int],
                      _prob : Int = 0, _action : Int = 0)
             extends Rule(_prob, _action) {

    def probability(p : Int) : Rule = copy(_prob = p)
    def action(act : Int) : Rule = copy(_action = act)

    def rewrite(w : Word, len : Int)
               (implicit alph : Alphabet) : Iterator[(Word, Int, Int)] = {
      for (i <- (0 until len).iterator;
           if pattern.matchesCirc(w, len, i).isDefined) yield {
        val newW =
          if (i + replacement.size > len) {
            val p = len - i
            update(update(w, i, replacement take p), 0, replacement drop p)
          } else {
            update(w, i, replacement)
          }
        (newW, prob, action)
      }
    }
  }

  //////////////////////////////////////////////////////////////////////////////

  def rewrite(w : Word, len : Int, probDenom : Int,
              actionAlph : Alphabet, rules : Seq[Rule])
             (implicit alph : Alphabet) : Iterator[(Word, Int, Int)] = {
    val post = for (r <- rules.iterator; p <- r.rewrite(w, len)) yield p

    if (verifyProbs) {
      val res = post.toList
      val actions = (res map (_._3)).toSet
      for (a <- actions) {
        val probSum = (for ((_, p, `a`) <- res) yield p).sum
        if (probSum != probDenom && probSum != 0)
          throw new Exception("Probability of transition from " + pp(w, len) +
                              " with action " + (actionAlph pp a) +
                              " is " + probSum + "/" + probDenom + " != 1")
      }
      res.iterator
    } else {
      post
    }
  }

}
