
package probsim

import common.VerificationUltility
import common.finiteautomata.{AutomataConverter, MonaUtility}
import probsim.ExplicitStateBisimulation.StateExtractor
import probsim.lstar._
import probsim.sat._
import probsim.systems._

object Main extends App {

  def newPart = {
    println
    println("=================================================================")
  }

  object Example extends Enumeration {
    val DCP2_print, DCP2_MB_print, GRADEv3_LSTAR = Value
  }

  for (chosenExample <- List(
    Example.DCP2_print,     // DCP
    Example.DCP2_MB_print,  // generalised DCP
    Example.GRADEv3_LSTAR   // the grade protocol
  )) try {
    time(chosenExample match {

      case Example.GRADEv3_LSTAR
      => {
        newPart
        println("Grade protocol v3 (with reference system)")
        object Alphabet extends Enumeration {
          val _, F, T, A, B, D, E, P = Value
        }
        object Actions extends Enumeration {
          val add_minus, toss0, toss1, reset, printT, printF, printE = Value
        }
        val prefix = "benchmark/grade-print-v3-2systems"
        val initConfigs = MonaUtility.loadMonaDFA(prefix + ".init.aut", false)
        val add_minus = MonaUtility.loadMonaDFA(prefix + ".add_minus.aut", true)
        val toss0 = MonaUtility.loadMonaDFA(prefix + ".toss0.aut", true)
        val toss1 = MonaUtility.loadMonaDFA(prefix + ".toss1.aut", true)
        val reset = MonaUtility.loadMonaDFA(prefix + ".reset.aut", true)
        val printT = MonaUtility.loadMonaDFA(prefix + ".printT.aut", true)
        val printF = MonaUtility.loadMonaDFA(prefix + ".printF.aut", true)
        val printE = MonaUtility.loadMonaDFA(prefix + ".printE.aut", true)
        val grade = new RegularPTS(initConfigs,
          Array(add_minus, toss0, toss1, reset, printT, printF, printE),
          Actions, Array(1, 2, 2, 2, 2, 2, 2), 2, Alphabet)
        new BisimulationStarWithMona(grade, prefix)
      }

      case Example.DCP2_print
      => {
        newPart
        println("Dining cryptographer (with reference system)")
        object Alphabet extends Enumeration {
          val _, F, T, A, B, C, D, P = Value
        }
        object Actions extends Enumeration {
          val xor, toss0, toss1, printT, printF = Value
        }
        val prefix = "benchmark/dining-print-2systems"
        val initConfigs = MonaUtility.loadMonaDFA(prefix + ".init.aut", false)
        val xor = MonaUtility.loadMonaDFA(prefix + ".xor.aut", true)
        val toss0 = MonaUtility.loadMonaDFA(prefix + ".toss0.aut", true)
        val toss1 = MonaUtility.loadMonaDFA(prefix + ".toss1.aut", true)
        val printT = MonaUtility.loadMonaDFA(prefix + ".printT.aut", true)
        val printF = MonaUtility.loadMonaDFA(prefix + ".printF.aut", true)
        val dining = new RegularPTS(initConfigs,
          Array(xor, toss0, toss1, printT, printF),
          Actions, Array(1, 2, 2, 2, 2), 2, Alphabet)
        new BisimulationStarWithMona(dining, prefix,
          ExplicitStateBisimulation.UnionSystemDistinctDeadEndStateExtractor(dining))
      }

      case Example.DCP2_MB_print
      => {
        newPart
        println("Dining cryptographer multi-bits (with reference system)")
        object Alphabet extends Enumeration {
          val _, F, T, A, B, D, E, P = Value
        }
        object Actions extends Enumeration {
          val xor, toss0, toss1, reset, printT, printF, printE = Value
        }
        val prefix = "benchmark/dining-mb-print-v3-2systems"
        val initConfigs = MonaUtility.loadMonaDFA(prefix + ".init.aut", false)
        val xor = MonaUtility.loadMonaDFA(prefix + ".xor.aut", true)
        val toss0 = MonaUtility.loadMonaDFA(prefix + ".toss0.aut", true)
        val toss1 = MonaUtility.loadMonaDFA(prefix + ".toss1.aut", true)
        val reset = MonaUtility.loadMonaDFA(prefix + ".reset.aut", true)
        val printT = MonaUtility.loadMonaDFA(prefix + ".printT.aut", true)
        val printF = MonaUtility.loadMonaDFA(prefix + ".printF.aut", true)
        val printE = MonaUtility.loadMonaDFA(prefix + ".printE.aut", true)
        val dining = new RegularPTS(initConfigs,
          //          Array(xor, toss0, toss1, reset), Actions,
          //          Array(1, 2, 2, 2), 2, Alphabet)
          Array(xor, toss0, toss1, reset, printT, printF, printE), Actions,
          Array(1, 2, 2, 2, 2, 2, 2), 2, Alphabet)
        new BisimulationStarWithMona(dining, prefix,
          ExplicitStateBisimulation.UnionSystemDistinctDeadEndStateExtractor(dining))
      }
    })
  }
  catch {
    case t: Throwable => t.printStackTrace
  }

  def LStarLoop(system: ExplicitStateSystem,
                prefix: String,
                bitWidth: Int,
                exposedStateInformation: StateExtractor = ExplicitStateBisimulation.ZeroStateExtractor,
                lenUpperBnd: Int = 5): Unit = {

    var lowerBnd = 0
    var upperBnd = lenUpperBnd
    val reachabilityCache = new ReachabilityBank(system)
    val cache = new BisimulationBank(system,
      exposedStateInformation,
      reachabilityCache)

    while (true) {
      println("==== Maximal length of words is set to " + upperBnd + " ====")

      val bisim = new BisimulationStar(system,
        exposedStateInformation, Some(cache), upperBnd, lowerBnd)
      val hypothesis = bisim.result

      println("Verifying with Mona...")

      MonaUtility.writeMonaDFA(prefix + ".proof.dfa", hypothesis, bitWidth, true)
      Thread.sleep(1000)

      val cex = new java.util.ArrayList[Integer]()
      if (!MonaUtility.getCexFromTeacher(prefix + ".teacher.mona", bitWidth, cex, true)) {
        println("Error: something wrong with Mona")
        System.exit(-1)
      }

      import FixedLengthWordOps._
      implicit val letters = system.letters

      if (cex.size() == 0) {
        println("The hypothesis passes the Mona test!")
        System.exit(0)
      } else {
        val (w1, w2, len) = decodeWordPair(cex)
        val cexMsg = "(" + FixedLengthWordOps.pp(w1, len) +
          ", " + FixedLengthWordOps.pp(w2, len) + ")"
        println("Mona gives a counterexample: " + cexMsg)

        lowerBnd = 1
        upperBnd = Math.max(lowerBnd + 1, upperBnd + 1)
      }
    }
  }

  def time[R](block: => R): R = {
    val t0 = System.currentTimeMillis()
    val result = block // call-by-name
    val t1 = System.currentTimeMillis()
    println("Elapsed time: " + (t1 - t0) / 1000 + " sec.")
    result
  }
}
