
package probsim

/**
 * Transform any system to an equivalent system over a binary alphabet.
 */
case class BinarisedSystem(oriSystem : ExplicitStateSystem)
           extends ExplicitStateSystem {

  import FixedLengthWordOps._

  val actions = oriSystem.actions
  val letters = NBitAlphabet(1)

  val probDenom = oriSystem.probDenom

  private val oriLetterWidth = oriSystem.letters.bitwidth

  def initialConfigurations(len : Int) : Iterator[Word] =
    if (len % oriLetterWidth == 0)
      oriSystem initialConfigurations (len / oriLetterWidth)
    else
      Iterator.empty

  def step(config : Word, len : Int) : Iterator[(Word, Int, Int)] =
    if (len % oriLetterWidth == 0)
      oriSystem.step(config, len / oriLetterWidth)
    else
      Iterator.empty

}