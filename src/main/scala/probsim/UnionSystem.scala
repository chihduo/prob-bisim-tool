
package probsim

/**
  * Union of two systems; this will group together instances of the
  * same length <code>len</code> into instances of length <code>len + 1</code>
  */
case class UnionSystem(left: ExplicitStateSystem,
                       right: ExplicitStateSystem,
                       actionLetters: Enumeration,
                       configLetters: Enumeration)
  extends ExplicitStateSystem {

  import FixedLengthWordOps._

  val actions = ActionAlphabet(actionLetters)
  val letters = LetterAlphabet(configLetters)

  assert(letters.size > 0)
  assert(left.actions.size == right.actions.size)
  assert(left.actions.size == actions.size)

  val (probDenom, leftProbFactor, rightProbFactor) =
    if (left.probDenom == right.probDenom)
      (left.probDenom, 1, 1)
    else
      (left.probDenom * right.probDenom, right.probDenom, left.probDenom)

  def left2Common(w: Word, len: Int): Word =
    update(transcode(w, len)(left.letters, letters), len, 0)(letters)

  def right2Common(w: Word, len: Int): Word =
    update(transcode(w, len)(right.letters, letters), len, 1)(letters)

  def common2LeftRight(w: Word, len: Int): Either[Word, Word] =
    ch(w, len - 1)(letters) match {
      case 0 => Left(transcode(w, len - 1)(letters, left.letters))
      case 1 => Right(transcode(w, len - 1)(letters, right.letters))
    }

  def initialConfigurations(len: Int): Iterator[Word] =
    if (len < 1)
      Iterator.empty
    else
      (for (w <- left.initialConfigurations(len - 1))
        yield left2Common(w, len - 1)) ++
        (for (w <- right.initialConfigurations(len - 1))
          yield right2Common(w, len - 1))

  def step(config: Word, len: Int): Iterator[(Word, Int, Int)] = {
    assert(len > 0)
    common2LeftRight(config, len) match {
      case Left(w) =>
        for ((w, p, a) <- left.step(w, len - 1))
          yield (left2Common(w, len - 1), p * leftProbFactor, a)
      case Right(w) =>
        for ((w, p, a) <- right.step(w, len - 1))
          yield (right2Common(w, len - 1), p * rightProbFactor, a)
    }
  }

}
