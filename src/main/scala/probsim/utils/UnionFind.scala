
package probsim.utils

import scala.collection.mutable.{HashMap => MHashMap, HashSet => MHashSet}

  class UnionFind[D] extends Cloneable {
    protected val parent = new MHashMap[D, D]
    protected val rank   = new MHashMap[D, Int]

    def apply(d : D) : D = {
      val p = parent(d)
      if (p == d) {
        p
      } else {
        val res = apply(p)
        parent.put(d, res)
        res
      }
    }

    def repr(d : D) : D =
      (parent get d) match {
        case None    => d
        case Some(p) => apply(p)
      }

    def contains(d : D) : Boolean =
      parent contains d

    def makeSet(d : D) : Unit = {
      parent.put(d, d)
      rank.put(d, 0)
    }

    def union(d : D, e : D) : Unit = {
      val dp = apply(d)
      val ep = apply(e)
      
      if (dp != ep) {
        val dr = rank(dp)
        val er = rank(ep)
        if (dr < er) {
          parent.put(dp, ep)
        } else if (dr > er) {
          parent.put(ep, dp)
        } else {
          parent.put(ep, dp)
          rank.put(dp, dr + 1)
        }
      }
    }

    override def clone : UnionFind[D] = {
      val res = new UnionFind[D]
      res.parent ++= this.parent
      res.rank ++= this.rank
      res
    }

    def representatives : Iterator[D] =
      for ((a, b) <- parent.iterator; if a == b) yield a

    def elements : Iterator[D] =
      for ((a, b) <- parent.iterator) yield a

    def classes : D => Iterable[D] = {
      val invertedForest = new MHashMap[D, D]
      for ((el, elParent) <- parent)
        if (el != elParent) {
          val elParent2 = apply(elParent)
          for (child <- invertedForest get elParent2)
            invertedForest.put(el, child)
          invertedForest.put(elParent2, el)
        }

      (el : D) => new Iterable[D] {
        def iterator = new Iterator[D] {
          var curEl : Option[D] = Some(el)
          def hasNext = curEl.isDefined
          def next = {
            val res = curEl.get
            curEl = invertedForest get res
            res
          }
        }
      }
    }

    override def toString : String = parent.toString
  }

////////////////////////////////////////////////////////////////////////////////

  class ProofProdUnionFind[D] extends UnionFind[D] {
    protected val directParent = new MHashMap[D, D]

    override def makeSet(d : D) : Unit = {
      super.makeSet(d)
      directParent.put(d, d)
    }

    override def union(d : D, e : D) : Unit = {
      val dp = apply(d)
      val ep = apply(e)
      
      if (dp != ep) {
        val dr = rank(dp)
        val er = rank(ep)
        if (dr < er) {
          invertParents(d, ep)
          directParent.put(d, e)
        } else if (dr > er) {
          invertParents(e, dp)
          directParent.put(e, d)
        } else {
          invertParents(e, dp)
          directParent.put(e, d)
          rank.put(dp, dr + 1)
        }
      }
    }

    private def invertParents(d : D, newSink : D) : Unit = {
      parent.put(d, newSink)
      val p = directParent(d)
      if (d != p) {
        invertParents(p, newSink)
        directParent.put(p, d)
      }
    }

    def getPath(d : D, e : D) : Seq[D] = {
      val dParents = new MHashSet[D]

      var cur = d
      var curParent = directParent(cur)

      dParents += cur

      while (cur != curParent) {
        dParents += curParent
        cur = curParent
        curParent = directParent(curParent)
      }

      var res1 = List[D](e)

      cur = e
      while (!(dParents contains cur)) {
        cur = directParent(cur)
        res1 = cur :: res1
      }

      var res2 = List[D]()

      var ds = d
      while (ds != cur) {
        res2 = ds :: res2
        ds = directParent(ds)
      }

      res2.reverse ::: res1
    }

    override def clone : ProofProdUnionFind[D] = {
      val res = new ProofProdUnionFind[D]
      res.parent ++= this.parent
      res.rank ++= this.rank
      res.directParent ++= this.directParent
      res
    }
  }
