
package probsim

////////////////////////////////////////////////////////////////////////////////

trait ExplicitStateSystem {
  import FixedLengthWordOps.Word

  val letters  : Alphabet
  val actions  : Alphabet

  // denominator of probability values
  val probDenom : Int

  // number of bits required to represent probability values
  lazy val probBitwidth : Int = {
    var w = 1
    while ((1 << (w - 1)) < probDenom)
      w = w + 1
    w
  }

  def initialConfigurations(len : Int) : Iterator[Word]

  // Given a configuration of a certain length, produce all configurations
  // (of the same length) reachable in one step. The returned tuples
  // also specify the probability of a transition, and the associated action
  def step(config : Word, len : Int) : Iterator[(Word, Int, Int)]
}

trait FixedLengthExplicitStateSystem extends ExplicitStateSystem {
  import FixedLengthWordOps.Word

  val wordLen : Int

  def initialConfigurations : Iterator[Word]

  def initialConfigurations(len : Int) : Iterator[Word] = len match {
    case `wordLen` => initialConfigurations
    case _         => Iterator.empty
  }

  def step(config : Word) : Iterator[(Word, Int, Int)]

  def step(config : Word, len : Int) : Iterator[(Word, Int, Int)] = {
    assert(len == wordLen)
    step(config)
  }
}

case class ExplicitStateSystemInstance(underlying : ExplicitStateSystem,
                                       wordLen : Int)
     extends FixedLengthExplicitStateSystem {
  import FixedLengthWordOps.Word

  val letters = underlying.letters
  val actions = underlying.actions
  val probDenom = underlying.probDenom
  def step(config : Word) = underlying.step(config, wordLen)
  def initialConfigurations = underlying initialConfigurations wordLen
}