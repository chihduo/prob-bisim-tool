# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/cygdrive/c/tmp/dfa-translator/src/BDD/bdd.c" "/cygdrive/c/tmp/dfa-translator/src/CMakeFiles/translator.dir/BDD/bdd.c.o"
  "/cygdrive/c/tmp/dfa-translator/src/BDD/bdd_cache.c" "/cygdrive/c/tmp/dfa-translator/src/CMakeFiles/translator.dir/BDD/bdd_cache.c.o"
  "/cygdrive/c/tmp/dfa-translator/src/BDD/bdd_double.c" "/cygdrive/c/tmp/dfa-translator/src/CMakeFiles/translator.dir/BDD/bdd_double.c.o"
  "/cygdrive/c/tmp/dfa-translator/src/BDD/bdd_dump.c" "/cygdrive/c/tmp/dfa-translator/src/CMakeFiles/translator.dir/BDD/bdd_dump.c.o"
  "/cygdrive/c/tmp/dfa-translator/src/BDD/bdd_external.c" "/cygdrive/c/tmp/dfa-translator/src/CMakeFiles/translator.dir/BDD/bdd_external.c.o"
  "/cygdrive/c/tmp/dfa-translator/src/BDD/bdd_manager.c" "/cygdrive/c/tmp/dfa-translator/src/CMakeFiles/translator.dir/BDD/bdd_manager.c.o"
  "/cygdrive/c/tmp/dfa-translator/src/BDD/bdd_trace.c" "/cygdrive/c/tmp/dfa-translator/src/CMakeFiles/translator.dir/BDD/bdd_trace.c.o"
  "/cygdrive/c/tmp/dfa-translator/src/BDD/hash.c" "/cygdrive/c/tmp/dfa-translator/src/CMakeFiles/translator.dir/BDD/hash.c.o"
  "/cygdrive/c/tmp/dfa-translator/src/DFA/analyze.c" "/cygdrive/c/tmp/dfa-translator/src/CMakeFiles/translator.dir/DFA/analyze.c.o"
  "/cygdrive/c/tmp/dfa-translator/src/DFA/basic.c" "/cygdrive/c/tmp/dfa-translator/src/CMakeFiles/translator.dir/DFA/basic.c.o"
  "/cygdrive/c/tmp/dfa-translator/src/DFA/dfa.c" "/cygdrive/c/tmp/dfa-translator/src/CMakeFiles/translator.dir/DFA/dfa.c.o"
  "/cygdrive/c/tmp/dfa-translator/src/DFA/external.c" "/cygdrive/c/tmp/dfa-translator/src/CMakeFiles/translator.dir/DFA/external.c.o"
  "/cygdrive/c/tmp/dfa-translator/src/DFA/makebasic.c" "/cygdrive/c/tmp/dfa-translator/src/CMakeFiles/translator.dir/DFA/makebasic.c.o"
  "/cygdrive/c/tmp/dfa-translator/src/DFA/minimize.c" "/cygdrive/c/tmp/dfa-translator/src/CMakeFiles/translator.dir/DFA/minimize.c.o"
  "/cygdrive/c/tmp/dfa-translator/src/DFA/prefix.c" "/cygdrive/c/tmp/dfa-translator/src/CMakeFiles/translator.dir/DFA/prefix.c.o"
  "/cygdrive/c/tmp/dfa-translator/src/DFA/printdfa.c" "/cygdrive/c/tmp/dfa-translator/src/CMakeFiles/translator.dir/DFA/printdfa.c.o"
  "/cygdrive/c/tmp/dfa-translator/src/DFA/product.c" "/cygdrive/c/tmp/dfa-translator/src/CMakeFiles/translator.dir/DFA/product.c.o"
  "/cygdrive/c/tmp/dfa-translator/src/DFA/project.c" "/cygdrive/c/tmp/dfa-translator/src/CMakeFiles/translator.dir/DFA/project.c.o"
  "/cygdrive/c/tmp/dfa-translator/src/DFA/quotient.c" "/cygdrive/c/tmp/dfa-translator/src/CMakeFiles/translator.dir/DFA/quotient.c.o"
  "/cygdrive/c/tmp/dfa-translator/src/Mem/dlmalloc.c" "/cygdrive/c/tmp/dfa-translator/src/CMakeFiles/translator.dir/Mem/dlmalloc.c.o"
  "/cygdrive/c/tmp/dfa-translator/src/Mem/mem.c" "/cygdrive/c/tmp/dfa-translator/src/CMakeFiles/translator.dir/Mem/mem.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  )
set(CMAKE_DEPENDS_CHECK_CXX
  "/cygdrive/c/tmp/dfa-translator/src/main.cpp" "/cygdrive/c/tmp/dfa-translator/src/CMakeFiles/translator.dir/main.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
