using namespace std;

extern "C" {
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "DFA/dfa.h"
#include "Mem/mem.h"
}
#include <fstream>
#include <iostream>
#include <string>

bool read(istream& is, string& line) {
	do {
		if (!getline(is, line))
			return false;
	} while (line[0] == '#');
	return true;
}

int main(int argc, char *argv[])
{
	if (argc != 2) {
		printf("usage: %s <output-filename>\n", argv[0]);
		exit(-1);
	}
	string line;
	int num_states, num_bits, sink_sid;

	//ifstream fin(argv[1]);

	// number of states (plus the boolvar state and the sink state)
	read(cin, line);
	num_states = stoi(line) + 2;
	sink_sid = num_states - 1;

	// number of bits
	read(cin, line);
	num_bits = stoi(line);

	if (num_bits > 9) {
		printf("error: too many bits (9 < %d)\n", num_bits);
		exit(-1);
	}

	unsigned int* var_index = new unsigned int[num_bits];
	char** var_names = new char*[num_bits];
	char* orders = new char[num_bits];
	for (size_t i = 0; i < num_bits; i++) {
		orders[i] = 2;
		var_index[i] = i;
		var_names[i] = new char[3]{ 'W' , (char)('1' + i), '\0' };
	}

	dfaSetup(num_states, num_bits, var_index);

	// create the boolvar state (sid = 0)
	dfaAllocExceptions(0);
	dfaStoreState(1);

	// read outgoing transitions for each state
	for (int from = 1; from < sink_sid; from++) {

		// num of successors of this state
		read(cin, line);
		int num_succ = stoi(line);

		dfaAllocExceptions(num_succ);

		for (int i = 0; i < num_succ; i++) {

			// the id of the i-th successor
			read(cin, line);

			// each state id is shifted by one
			int to = stoi(line) + 1;

			// the label for the i-th successor
			read(cin, line);

			dfaStoreException(to, line.c_str());
		}

		// set default to the sink state
		dfaStoreState(sink_sid);
	}

	// create the sink state
	dfaAllocExceptions(0);
	dfaStoreState(sink_sid);

	// read statuses of the states
	read(cin, line);

	// note that we need to shift each state status by one
	// and inserted the dont-care status at the beginning
	char* statues = new char[num_states + 1];
	strncpy(statues + 1, line.c_str(), num_states - 2);
	statues[0] = '0';
	statues[sink_sid] = '-';
	statues[num_states] = '\0';

	//printf("status: %s", statues);

	DFA* dfa = dfaBuild(statues);
	//DFA* dfa2 = dfaMinimize(dfa);

	const char* ext = strrchr(argv[1], '.');
	if (!strcmp(ext, ".dfa")) {
		dfaExport(dfa, argv[1], num_bits, var_names, orders);
	}
	else if (!strcmp(ext, ".aut")) {
		dfaPrint(dfa, argv[1], num_bits, var_names, var_index);
	}
	else {
		printf("Unsupported format: %s\n", ext);
		exit(-1);
	}

	dfaFree(dfa);
	//dfaFree(dfa2);
	mem_free(var_index);
	for (size_t i = 0; i < num_bits; i++) {
		mem_free(var_names[i]);
	}
	mem_free(statues);
	return 0;
}

int decode_example(char *example, int row, int num_rows)
{
	/* decode one row of the example string */
	int i, val = 0, length = strlen(example) / (num_rows + 1);
	for (i = length - 1; i > 0; i--)
		val = val << 1 | (example[row*length + i] == '1');
	return val;
}